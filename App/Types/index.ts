export type DataItemType = {
  [key: string]: any;
};
