import { createTheme } from '@mui/material/styles';
import { red } from '@mui/material/colors';
import { experimental_extendTheme as extendMuiTheme } from '@mui/material/styles';

// Create a theme instance.
const loginTheme = extendMuiTheme({
  colorSchemes: {
    light: {
      palette: {
        primary: {
          main: '#027FFF',
          //   dark: '#212529',
        },
        secondary: {
          main: '#0E51AA',
        },
        error: {
          main: red.A400,
        },
        background: {
          default: '#027FFF',
        },
      },
    },
  },
});

export default loginTheme;
