import { extendTheme } from '@mui/joy/styles';
import colors from '@mui/material/colors';
import { deepmerge } from '@mui/utils';
import {
  useColorScheme,
  Experimental_CssVarsProvider as CssVarsProvider,
  experimental_extendTheme as extendMuiTheme,
} from '@mui/material/styles';
import { extendTheme as extendJoyTheme } from '@mui/joy/styles';

// Create a theme instance.
const joyTheme = extendTheme({
  // This is required to point to `var(--mui-*)` because we are using `CssVarsProvider` from Material UI.
  cssVarPrefix: 'mui',
  colorSchemes: {
    light: {
      palette: {
        primary: {
          solidColor: 'var(--mui-palette-primary-contrastText)',
          solidBg: 'var(--mui-palette-primary-main)',
          solidHoverBg: 'var(--mui-palette-primary-dark)',
          plainColor: 'var(--mui-palette-primary-main)',
          plainHoverBg:
            'rgba(var(--mui-palette-primary-mainChannel) / var(--mui-palette-action-hoverOpacity))',
          plainActiveBg: 'rgba(var(--mui-palette-primary-mainChannel) / 0.3)',
          outlinedBorder: 'rgba(var(--mui-palette-primary-mainChannel) / 0.5)',
          outlinedColor: 'var(--mui-palette-primary-main)',
          outlinedHoverBg:
            'rgba(var(--mui-palette-primary-mainChannel) / var(--mui-palette-action-hoverOpacity))',
          outlinedHoverBorder: 'var(--mui-palette-primary-main)',
          outlinedActiveBg: 'rgba(var(--mui-palette-primary-mainChannel) / 0.3)',
        },
        neutral: {},
        // Do the same for the `danger`, `info`, `success`, and `warning` palettes,
        divider: 'var(--mui-palette-divider)',
        text: {
          tertiary: 'rgba(0 0 0 / 0.56)',
        },
      },
    },
    // Do the same for dark mode
    // dark: { ... }
  },
  radius: {
    xs: '2px',
    sm: '4px',
    md: '8px',
    lg: '12px',
    xl: '16px',
  },
  components: {
    JoyTextField: {
      defaultProps: {
        componentsProps: {
          label: {
            style: {
              fontWeight: 'normal',
            },
          },
          // Prevent Validation tidak merah
          //   input: {
          //     style: {
          //       fontSize: '13px',
          //       borderRadius: '0.25rem',
          //       borderColor: 'var(--mui-palette-neutral-200)',
          //       border: '1px solid var(--mui-palette-neutral-200)',
          //       color: 'var(--mui-palette-neutral-700)',
          //       outline: '0px !important',
          //       WebkitAppearance: 'none',
          //       boxShadow: 'none !important',
          //       // paddingLeft: '1rem',
          //       backgroundClip: 'padding-box',
          //       appearance: 'none',
          //       transition: 'border-color .15s ease-in-out, box-shadow .15s ease-in-out',
          //     },
          //   },
        },
      },
    },
  },
  fontFamily: {
    display: '"Roboto","Helvetica","Arial",sans-serif',
    body: '"Roboto","Helvetica","Arial",sans-serif',
  },
});

export default joyTheme;
