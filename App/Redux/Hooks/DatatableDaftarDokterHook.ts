import { useDispatch, useSelector } from 'react-redux';
import type { ColumnFiltersState, PaginationState, SortingState } from '@tanstack/react-table';

import DaftarDokterService from 'App/Services/Master/DaftarDokterService';
import {
  setDataDaftarDokter,
  getDataDaftarDokter,
  setPaginationDaftarDokter,
  getPaginationDaftarDokter,
  setMetaDaftarDokter,
  getMetaDaftarDokter,
  setIsLoadingDaftarDokter,
  getIsLoadingDaftarDokter,
  setIsErrorDaftarDokter,
  getIsErrorDaftarDokter,
  setIsRefetchingDaftarDokter,
  getIsRefetchingDaftarDokter,
  setSearchDaftarDokter,
  getSearchDaftarDokter,
  setSortingDaftarDokter,
  getSortingDaftarDokter,
  getFiltersDaftarDokter,
  setFiltersDaftarDokter,
  initialState,
} from 'App/Redux/Slices/DatatableDaftarDokterSlice';
import {
  getNormalizeFiltersEqual,
  getNormalizeSorting,
} from 'App/Helpers/QueryParams/NextPaginateQueryParams';

export type TFetchData = {
  pagination?: PaginationState | null | undefined;
  globalFilter?: string | null | undefined;
  columnFilters?: ColumnFiltersState | null | undefined;
  sorting?: SortingState | null | undefined;
};

const DatatableDaftarDokterHook = () => {
  // Datatables
  const dispatch = useDispatch();
  const datatableDataDaftarDokter = useSelector(getDataDaftarDokter);
  const datatableMetaDaftarDokter = useSelector(getMetaDaftarDokter);
  const datatablePaginationDaftarDokter = useSelector(getPaginationDaftarDokter);
  const datatableSearchDaftarDokter = useSelector(getSearchDaftarDokter);
  const datatableFiltersDaftarDokter = useSelector(getFiltersDaftarDokter);
  const datatableSortingDaftarDokter = useSelector(getSortingDaftarDokter);
  const datatableIsLoadingDaftarDokter = useSelector(getIsLoadingDaftarDokter);
  const datatableIsErrorDaftarDokter = useSelector(getIsErrorDaftarDokter);
  const datatableIsRefetchingDaftarDokter = useSelector(getIsRefetchingDaftarDokter);

  const fetchData = async (queryParams?: TFetchData) => {
    const tempPagination = queryParams?.pagination ?? datatablePaginationDaftarDokter;
    const tempGlobalFilter = queryParams?.globalFilter ?? datatableSearchDaftarDokter;
    let tempFilters: any = queryParams?.columnFilters ?? datatableFiltersDaftarDokter;
    let tempSorting: any = queryParams?.sorting ?? datatableSortingDaftarDokter;
    tempFilters = getNormalizeFiltersEqual(tempFilters);
    tempSorting = getNormalizeSorting(tempSorting);
    if (datatableDataDaftarDokter.length < 1) {
      dispatch(setIsLoadingDaftarDokter(true));
    } else {
      dispatch(setIsRefetchingDaftarDokter(true));
    }
    const resResult = await DaftarDokterService.getPagination({
      page: tempPagination.pageIndex + 1,
      limit: tempPagination.pageSize,
      search: tempGlobalFilter,
      ...tempFilters,
      ...tempSorting,
    });
    if (resResult?.success) {
      const resData = resResult.data;
      const pagination: PaginationState = {
        pageIndex: resData.meta.currentPage - 1,
        pageSize: resData.meta.itemsPerPage,
      };
      dispatch(setDataDaftarDokter(resData.data));
      dispatch(setMetaDaftarDokter(resData.meta));
      dispatch(setPaginationDaftarDokter(pagination));
      dispatch(setIsErrorDaftarDokter(false));
    } else {
      dispatch(setIsErrorDaftarDokter(true));
    }
    dispatch(setIsLoadingDaftarDokter(false));
    dispatch(setIsRefetchingDaftarDokter(false));
  };

  const handleSearch = (value: any) => {
    dispatch(setSearchDaftarDokter(value));
    fetchData({
      pagination: initialState.pagination,
      globalFilter: value,
    });
  };
  const handleSorting = (setterCallback: any) => {
    const resultCallback: SortingState = setterCallback(datatableSortingDaftarDokter);
    dispatch(setSortingDaftarDokter([...resultCallback]));
    fetchData({
      sorting: [...resultCallback],
    });
  };
  const handleFilters = (setterCallback: any) => {
    const resultCallback: ColumnFiltersState = setterCallback(datatableFiltersDaftarDokter);
    dispatch(setFiltersDaftarDokter(resultCallback));
    fetchData({
      pagination: initialState.pagination,
      columnFilters: [...resultCallback],
    });
  };
  const handlePagination = (setterCallback: any) => {
    const resultCallback: PaginationState = setterCallback(datatablePaginationDaftarDokter);
    dispatch(setPaginationDaftarDokter({ ...resultCallback }));
    fetchData({
      pagination: { ...resultCallback },
    });
  };
  const handleCustomFilterHeader = async (values: any) => {
    let columnFilters = [];
    if (values.status == '1' || values.status == '0') {
      let isActiveFilter = {
        id: 'is_active',
        value: values.status == '1' ? true : false,
      };
      columnFilters.push(isActiveFilter);
    }
    dispatch(setSearchDaftarDokter(values.globalFilter));
    dispatch(setFiltersDaftarDokter(columnFilters));
    fetchData({
      pagination: initialState.pagination,
      globalFilter: values.globalFilter,
      columnFilters: columnFilters,
    });
  };
  const handleResetFilterHeader = async () => {
    dispatch(setSearchDaftarDokter(''));
    dispatch(setFiltersDaftarDokter([]));
    fetchData({
      pagination: initialState.pagination,
      sorting: initialState.sorting,
      globalFilter: '',
      columnFilters: [],
    });
  };

  return {
    datatableDataDaftarDokter,
    datatableMetaDaftarDokter,
    datatablePaginationDaftarDokter,
    datatableSearchDaftarDokter,
    datatableFiltersDaftarDokter,
    datatableSortingDaftarDokter,
    datatableIsLoadingDaftarDokter,
    datatableIsErrorDaftarDokter,
    datatableIsRefetchingDaftarDokter,
    handleSearch,
    handleFilters,
    handleSorting,
    handlePagination,
    fetchData,
    handleCustomFilterHeader,
    handleResetFilterHeader,
  };
};

export default DatatableDaftarDokterHook;