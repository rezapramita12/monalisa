import { createSlice, Draft, PayloadAction } from '@reduxjs/toolkit';

export type TTindakan = {};

export type TListTindakan = any;

const initialState: TListTindakan = [];

export const TindakanSlice = createSlice({
  name: 'listTindakan',
  initialState,
  reducers: {
    setListTindakan: (
      state: Draft<typeof initialState>,
      action: PayloadAction<typeof initialState>,
    ) => {
      const tmp = action.payload;
      state = [];
      return (state = [...state, ...tmp]);
    },
  },
});

// Selectors
export const getListTindakan = (state: any) => state.listTindakan;

// Reducers and actions
export const { setListTindakan } = TindakanSlice.actions;

export default TindakanSlice.reducer;
