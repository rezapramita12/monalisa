import { createSlice, Draft, PayloadAction } from '@reduxjs/toolkit';

export type TMenuPengaturan = {
  id: string | number;
  name: string;
  isParent: boolean;
  parentActiveLink?: string;
  link?: string;
  icon?: string;
  childMenus?: Array<TMenuPengaturan>;
};

export type TMenuPengaturans = Array<TMenuPengaturan>;

// TODOS: sementara
const initialState: TMenuPengaturans = [
  {
    id: 15,
    name: 'Pengaturan',
    isParent: true,
    icon: 'menu-icon-master',
    link: '/admin/master',
  },
  {
    id: 16,
    name: 'Bantuan',
    isParent: true,
    icon: 'menu-icon-master',
    link: '/admin/master',
  },
];

export const menuPengaturansSlice = createSlice({
  name: 'menuPengaturans',
  initialState,
  reducers: {
    setMenuPengaturans: (
      state: Draft<typeof initialState>,
      action: PayloadAction<typeof initialState>,
    ) => {
      const tempMenus = action.payload;
      state = [...state, ...tempMenus];
    },
    resetUser: (state: Draft<typeof initialState>) => {
      state = [];
    },
  },
});

// Selectors
export const getMenuPengaturans = (state: any) => state.menuPengaturans;

// Reducers and actions
export const { setMenuPengaturans } = menuPengaturansSlice.actions;

export default menuPengaturansSlice.reducer;
