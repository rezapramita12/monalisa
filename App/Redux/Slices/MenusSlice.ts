import { createSlice, Draft, PayloadAction } from '@reduxjs/toolkit';

export type TMenu = {
  id: string | number;
  name: string;
  isParent: boolean;
  parentActiveLink?: string;
  link?: string;
  icon?: string;
  childMenus?: Array<TMenu>;
};

export type TMenus = Array<TMenu>;

// TODOS: sementara
const initialState: TMenus = [
  {
    id: 1,
    name: 'Dashboard',
    icon: 'menu-icon-dashboard',
    link: '/admin',
    parentActiveLink: '/admin',
    isParent: false,
  },
  {
    id: 2,
    name: 'Mahasiswa',
    isParent: true,
    icon: 'menu-icon-registration',
    parentActiveLink: '/admin/pendaftaran',
    childMenus: [
      {
        id: 3,
        name: 'Pendaftaran',
        link: '/admin/pendaftaran',
        isParent: false,
      },
      {
        id: 4,
        name: 'Pendaftaran Rawat Jalan',
        link: '/admin/pendaftaran/rawat-jalan',
        isParent: false,
      },
    ],
  },
  {
    id: 5,
    name: 'Guru',
    isParent: true,
    icon: 'menu-icon-master',
    parentActiveLink: '/admin/master',
    childMenus: [
      {
        id: 6,
        name: 'Daftar Dokter',
        link: '/admin/master/daftar-dokter',
        isParent: false,
      },
      {
        id: 7,
        name: 'Jadwal Dokter',
        link: '/admin/master/jadwal-dokter',
        isParent: false,
      },
    ],
  },
  {
    id: 8,
    name: 'Perpus',
    isParent: true,
    icon: 'menu-icon-master',
    parentActiveLink: '/admin/master',
    childMenus: [
      {
        id: 9,
        name: 'Anggota',
        link: '/admin/master/daftar-dokter',
        isParent: false,
      },
      {
        id: 10,
        name: 'Buku',
        link: '/admin/master/jadwal-dokter',
        isParent: false,
      },
    ],
  },
  {
    id: 11,
    name: 'Keuangan',
    isParent: true,
    icon: 'menu-icon-master',
    link: '/admin/master',
  },
  {
    id: 12,
    name: 'Broadcast',
    isParent: true,
    icon: 'menu-icon-master',
    link: '/admin/master',
  },
  {
    id: 13,
    name: 'Pesan',
    isParent: true,
    icon: 'menu-icon-master',
    link: '/admin/master',
  },
  {
    id: 14,
    name: 'Berita&Beasiswa',
    isParent: true,
    icon: 'menu-icon-master',
    link: '/admin/master',
  },
];

export const menusSlice = createSlice({
  name: 'menus',
  initialState,
  reducers: {
    setMenus: (state: Draft<typeof initialState>, action: PayloadAction<typeof initialState>) => {
      const tempMenus = action.payload;
      state = [...state, ...tempMenus];
    },
    resetUser: (state: Draft<typeof initialState>) => {
      state = [];
    },
  },
});

// Selectors
export const getMenus = (state: any) => state.menus;

// Reducers and actions
export const { setMenus } = menusSlice.actions;

export default menusSlice.reducer;
