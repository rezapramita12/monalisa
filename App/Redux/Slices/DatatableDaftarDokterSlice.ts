import { createSlice, Draft, PayloadAction } from '@reduxjs/toolkit';
import type {
  ColumnFiltersState,
  Updater,
  PaginationState,
  SortingState,
} from '@tanstack/react-table';

export type TDatatableDaftarDokter = {
  data: Array<any>;
  meta: TDatatableMeta | null;
  filters: ColumnFiltersState;
  sorting: SortingState;
  pagination: PaginationState;
  search: string | null;
  url: null;
  isLoading: boolean;
  isRefetching: boolean;
  isError: boolean;
};

export type TDatatableMeta = {
  itemsPerPage: number | string | any;
  totalItems: number | string | any;
  currentPage: number | string | any;
  totalPages: number | string | any;
  sortBy: Array<any>;
};

export type TDataDaftarDokter = TDatatableDaftarDokter;

export const initialState: TDataDaftarDokter = {
  data: [],
  meta: {
    itemsPerPage: 0,
    totalItems: 0,
    currentPage: 1,
    totalPages: 0,
    sortBy: [],
  },
  filters: [],
  sorting: [],
  pagination: {
    pageIndex: 0,
    pageSize: 100,
  },
  search: null,
  url: null,
  isLoading: true,
  isRefetching: false,
  isError: false,
};

export const DatatableDaftarDokterSlice = createSlice({
  name: 'datatableDaftarDokter',
  initialState,
  reducers: {
    setDataDaftarDokter: (state: Draft<typeof initialState>, action: PayloadAction<Array<any>>) => {
      const tmp = action.payload;
      state.data = [];
      state.data = [...state.data, ...tmp];
      return state;
    },
    setMetaDaftarDokter: (
      state: Draft<typeof initialState>,
      action: PayloadAction<TDatatableMeta>,
    ) => {
      const tmp = action.payload;
      state.meta = {
        ...tmp,
      };
      return state;
    },
    setPaginationDaftarDokter: (
      state: Draft<typeof initialState>,
      action: PayloadAction<PaginationState>,
    ) => {
      const tmp = action.payload;
      state.pagination = {
        ...tmp,
      };
      return state;
    },
    setIsLoadingDaftarDokter: (
      state: Draft<typeof initialState>,
      action: PayloadAction<boolean>,
    ) => {
      const tmp = action.payload;
      state.isLoading = tmp;
      return state;
    },
    setIsRefetchingDaftarDokter: (
      state: Draft<typeof initialState>,
      action: PayloadAction<boolean>,
    ) => {
      const tmp = action.payload;
      state.isRefetching = tmp;
      return state;
    },
    setIsErrorDaftarDokter: (state: Draft<typeof initialState>, action: PayloadAction<boolean>) => {
      const tmp = action.payload;
      state.isError = tmp;
      return state;
    },
    setSearchDaftarDokter: (
      state: Draft<typeof initialState>,
      action: PayloadAction<string | null>,
    ) => {
      const tmp = action.payload;
      state.search = tmp;
      return state;
    },
    setSortingDaftarDokter: (
      state: Draft<typeof initialState>,
      action: PayloadAction<SortingState>,
    ) => {
      const tmp = action.payload;
      state.sorting = tmp;
      return state;
    },
    setFiltersDaftarDokter: (
      state: Draft<typeof initialState>,
      action: PayloadAction<ColumnFiltersState>,
    ) => {
      const tmp = action.payload;
      state.filters = tmp;
      return state;
    },
  },
});

// Selectors
export const getDataDaftarDokter = (state: any) => state.datatableDaftarDokter.data;
export const getMetaDaftarDokter = (state: any): TDatatableMeta => state.datatableDaftarDokter.meta;
export const getPaginationDaftarDokter = (state: any): PaginationState =>
  state.datatableDaftarDokter.pagination;
export const getIsLoadingDaftarDokter = (state: any): boolean =>
  state.datatableDaftarDokter.isLoading;
export const getIsRefetchingDaftarDokter = (state: any): boolean =>
  state.datatableDaftarDokter.isRefetching;
export const getIsErrorDaftarDokter = (state: any): boolean => state.datatableDaftarDokter.isError;
export const getSearchDaftarDokter = (state: any): string => state.datatableDaftarDokter.search;
export const getSortingDaftarDokter = (state: any): SortingState =>
  state.datatableDaftarDokter.sorting;
export const getFiltersDaftarDokter = (state: any): ColumnFiltersState =>
  state.datatableDaftarDokter.filters;

// Reducers and actions
export const {
  setDataDaftarDokter,
  setMetaDaftarDokter,
  setPaginationDaftarDokter,
  setIsLoadingDaftarDokter,
  setIsRefetchingDaftarDokter,
  setIsErrorDaftarDokter,
  setSearchDaftarDokter,
  setSortingDaftarDokter,
  setFiltersDaftarDokter,
} = DatatableDaftarDokterSlice.actions;

export default DatatableDaftarDokterSlice.reducer;
