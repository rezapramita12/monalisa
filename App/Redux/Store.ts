import { configureStore } from '@reduxjs/toolkit';

import userReducer from 'App/Redux/Slices/UserSlice';
import themeReducer from 'App/Redux/Slices/ThemeSlice';
import menusReducer from 'App/Redux/Slices/MenusSlice';
import menuPengaturansReducer from 'App/Redux/Slices/MenusPengaturanSlice';
import datatableDaftarDokterReducer from 'App/Redux/Slices/DatatableDaftarDokterSlice';
import listTindakanReducer from 'App/Redux/Slices/TindakanSlice';

export default configureStore({
  reducer: {
    user: userReducer,
    theme: themeReducer,
    menus: menusReducer,
    menuPengaturans: menuPengaturansReducer,
    listTindakan: listTindakanReducer,
    datatableDaftarDokter: datatableDaftarDokterReducer,
  },
});
