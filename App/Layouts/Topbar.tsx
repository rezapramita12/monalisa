/* eslint-disable @next/next/no-img-element */
import { AppBar, Box, Toolbar, Container } from '@mui/material';
import { styled } from '@mui/material/styles';
import { useTheme } from '@mui/material/styles';
import Typography from '@mui/material/Typography';

import DhSearchInput from 'App/Components/Input/SearchInput';
import DhProfile from './Components/Profile';
import DhNotification from './Components/Notification';
import Image from 'next/image';

const ResponsiveAppBar = () => {
  const theme = useTheme();

  const StyledAppBar: any = styled(AppBar)(({ theme }) => ({
    height: '40px',
    padding: 0,
    backgroundColor: theme.palette.secondary.main,
  }));
  const StyledToolbar = styled(Toolbar)(({ theme }) => ({
    height: '40px',
    marginLeft: '15px',
    padding: 0,
  }));
  const StyledContainer = styled(Container)(({ theme }) => ({
    height: '40px',
    margin: 0,
    padding: 0,
  }));

  return (
    <StyledAppBar elevation={0} position="static">
      <StyledContainer disableGutters maxWidth={false}>
        <StyledToolbar>
          <Typography
            noWrap
            component="a"
            href="/admin"
            sx={{
              mr: 2,
              fontSize: '16px',
              marginLeft: '0',
              marginTop: '-25px',
              display: { xs: 'none', sm: 'flex' },
              fontWeight: 700,
              color: 'inherit',
              textDecoration: 'none',
            }}
          >
            Monalisa
          </Typography>
          <Typography
            noWrap
            component="a"
            href=""
            sx={{
              mr: 2,
              fontSize: '16px',
              marginLeft: '0',
              marginTop: '-10px',
              flexGrow: 1,
              fontWeight: 700,
              color: 'inherit',
              textDecoration: 'none',
              display: { xs: 'flex', sm: 'none' },
            }}
          >
            Monalisa
          </Typography>
          <Box sx={{ flexGrow: 2, display: { xs: 'none', sm: 'flex' } }}></Box>
          <Box sx={{ display: { xs: 'flex', sm: 'none' } }}>
            <DhNotification />
            <DhProfile />
          </Box>
          <Box
            sx={{
              marginTop: '-25px',
              flexGrow: 1,
              justifyContent: 'end',
              display: { xs: 'none', sm: 'flex' },
            }}
          >
            <DhNotification />
            <DhProfile />
            <img
              src="/img/cliniqu-nav-biru.png"
              width="150px"
              alt="..."
              style={{ marginBottom: '4px', marginRight: '-23px' }}
            />
          </Box>
        </StyledToolbar>
      </StyledContainer>
    </StyledAppBar>
  );
};
export default ResponsiveAppBar;
