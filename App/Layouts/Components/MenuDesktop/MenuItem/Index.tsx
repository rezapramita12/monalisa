import { FC } from 'react';

import { TMenu } from 'App/Redux/Slices/MenusSlice';
import DoubleMenu from './DoubleMenu/Index';
import SingleMenu from './SingleMenu';

interface IProps extends TMenu {}

const DhComponent: FC<IProps> = (props: IProps) => {
  let { isParent } = props;

  if (isParent) {
    return <DoubleMenu {...props} />;
  }

  return <SingleMenu {...props} />;
};

export default DhComponent;
