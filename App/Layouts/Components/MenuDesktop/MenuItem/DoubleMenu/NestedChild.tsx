import { useState, MouseEvent, Fragment, FC, ReactNode, CSSProperties } from 'react';
import { useRouter } from 'next/router';
import MenuItem from '@mui/material/MenuItem';
import MenuList from '@mui/material/MenuList';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import ListItemIcon from '@mui/material/ListItemIcon';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { styled } from '@mui/material/styles';
import { colors } from '@material-ui/core';
import { useTheme } from '@mui/material/styles';

import { TMenu } from 'App/Redux/Slices/MenusSlice';
import NextLink from 'App/Components/Link/NextLink';

const DhComponent: FC<any> = (props: any) => {
  const router = useRouter();
  const theme = useTheme();
  let renderMenus = props.childMenus.map((item: any, index: any) => {
    const isParentActive = router.pathname.startsWith(item?.link);
    let styleSelectedMenu: any = {};
    let styleSelectedText: any = {};
    if (isParentActive) {
      styleSelectedMenu = {
        backgroundColor: theme.palette.primary.main,
        '&:hover': {
          backgroundColor: theme.palette.primary.dark,
        },
      };
      styleSelectedText = {
        color: 'white',
      };
    }
    return (
      <NextLink key={index} href={item.link} noLinkStyle>
        <MenuItem sx={styleSelectedMenu}>
          <ListItemText>
            <Typography sx={styleSelectedText} fontSize={14}>
              {item.name}
            </Typography>
          </ListItemText>
        </MenuItem>
      </NextLink>
    );
  });
  return <MenuList>{renderMenus}</MenuList>;
};

export default DhComponent;
