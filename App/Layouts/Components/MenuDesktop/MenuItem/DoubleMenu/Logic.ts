import { useState, MouseEvent } from 'react';
import { useRouter } from 'next/router';

import { useTheme } from '@mui/material/styles';

import { IProps } from './Index';

const Logic = (props: IProps) => {
  let { id, name, icon, parentActiveLink } = props;
  parentActiveLink = parentActiveLink ?? '';
  const router = useRouter();
  const theme = useTheme();
  const isParentActive = router.pathname.startsWith(parentActiveLink);
  let buttonCss: any = {
    marginRight: '10px',
    fontSize: '12px',
    height: '20px',
    marginTop: '-20px',
    paddingTop: '15px',
    paddingBottom: '15px',
    textTransform: 'none',
    color: theme.palette.text.disabled,
  };
  if (isParentActive) {
    buttonCss = {
      ...buttonCss,
      '& .MuiTypographyRoot.MuiTypographyBody1': {
        color: theme.palette.primary.main,
      },
    };
  }

  if (!icon) icon = 'iconHome';
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return {
    id,
    name,
    icon,
    anchorEl,
    setAnchorEl,
    open,
    handleClick,
    handleClose,
    isParentActive,
    buttonCss,
  };
};

export default Logic;