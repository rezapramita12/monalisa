import { useState, MouseEvent, Fragment, FC, ReactNode } from 'react';

import Menu from '@mui/material/Menu';
import Button from '@mui/material/Button';
import { colors } from '@mui/material';
import Typography from '@mui/material/Typography';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { CSSProperties } from '@material-ui/core/styles/withStyles';
import { useTheme } from '@mui/material/styles';

import { TMenu } from 'App/Redux/Slices/MenusSlice';
import IconParent from '../IconElementParent';
import NestedChild from './NestedChild';
import useLogic from './Logic';

export interface IProps extends TMenu {}

const DhComponent: FC<IProps> = (props: IProps) => {
  const { id, name, icon, anchorEl, buttonCss, open, handleClick, handleClose, isParentActive } =
    useLogic(props);

  const theme = useTheme();

  return (
    <Fragment>
      <Button
        id="basic-button"
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
        key={id}
        startIcon={<IconParent name={icon} isActive={isParentActive} />}
        endIcon={<ArrowDropDownIcon style={{ marginLeft: '-5px' }} />}
        sx={{
          ...buttonCss,
        }}
      >
        <Typography fontSize={14}>{name}</Typography>
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
        PaperProps={{
          style: {
            minWidth: '225px',
          },
        }}
      >
        <NestedChild {...props} />
      </Menu>
    </Fragment>
  );
};
export default DhComponent;
