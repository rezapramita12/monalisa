import { FC } from 'react';

import Typography from '@mui/material/Typography';
import { useRouter } from 'next/router';
import styled from '@emotion/styled';

import { TMenu } from 'App/Redux/Slices/MenusSlice';
import ButtonLink from 'App/Components/Link/ButtonLink';
import IconParent from './IconElementParent';

interface IProps extends TMenu { }

const DhComponent: FC<IProps> = (props: IProps) => {
  let { id, name, icon, link } = props;
  const router = useRouter();

  if (!icon) icon = 'iconHome';
  if (!link) link = '/admin';

  let activeCss: any = {};
  const isActiveLink = router.pathname == link;
  if (isActiveLink) {
    activeCss = {
      '& .MuiTypographyRoot.MuiTypographyBody1': {},
    };
  }

  const StyledButtonLink = styled(ButtonLink)(() => ({
    marginRight: '10px',
    fontSize: '12px',
    height: '20px',
    marginTop: '-20px',
    paddingTop: '15px',
    paddingBottom: '15px',
    textTransform: 'none',
    ...activeCss,
  }));

  return (
    <StyledButtonLink
      id="basic-button"
      style={activeCss}
      href={link}
      key={id}
      startIcon={<IconParent name={icon} isActive={isActiveLink} />}
    >
      <Typography fontSize={14}>{name}</Typography>
    </StyledButtonLink>
  );
};

export default DhComponent;
