import { FC } from 'react';

import { Icon } from '@mui/material';
import { useTheme } from '@mui/material/styles';

import DhSvg from 'App/Components/Icon';

interface IProps {
  name: string;
  isActive?: boolean;
}

const DhComponent: FC<IProps> = (props: IProps) => {
  const { name } = props;
  const isActive = props?.isActive;
  const theme = useTheme();

  return (
    <Icon
      sx={{
        marginTop: '-6px',
        marginRight: '-5px',
        color: isActive ? theme.palette.primary.main : theme.palette.text.disabled,
      }}
    >
      <DhSvg
        name={name}
        sx={{
          fontSize: '18px',
        }}
      />
    </Icon>
  );
};
export default DhComponent;
