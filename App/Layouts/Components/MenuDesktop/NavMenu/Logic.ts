/* eslint-disable import/no-anonymous-default-export */
import { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { getMenus, TMenus } from 'App/Redux/Slices/MenusSlice';
import { IProps } from './Index';

export default (props: IProps) => {
  const menus: TMenus = useSelector(getMenus);

  const initFunction = async () => {
    // Init Client Side
  };

  useEffect(() => {
    initFunction();
  }, []);

  return {
    menus,
  };
};
