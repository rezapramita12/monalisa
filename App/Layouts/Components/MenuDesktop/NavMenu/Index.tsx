import { FC, Fragment, ReactNode } from 'react';

import Box from '@mui/material/Box';
import { Grid } from '@material-ui/core';
import { TextField } from '@mui/joy';

import DhMenu from 'App/Layouts/Components/MenuDesktop/MenuItem/Index';
import useLogic from './Logic';

export type IProps = {
  children?: ReactNode;
};

const DhComponent: FC<IProps> = props => {
  const { menus } = useLogic(props);

  return (
    <Fragment>
      <Grid container direction="row" justifyContent="space-between" alignItems="center">
        <Box
          sx={{
            flexGrow: 1,
            display: { xs: 'none', md: 'flex' },
            marginLeft: '15px',
            marginTop: '-5px',
          }}
        >
          {menus.map((result, index) => (
            <DhMenu key={index} {...result} />
          ))}
        </Box>
        <TextField
          size="sm"
          placeholder="Cari No. Rm/nama pasien/tanggal lahir"
          sx={{
            width: 300,
            marginTop: '-22px',
            color: 'black',
            display: {
              xs: 'none',
              md: 'flex',
            },
          }}
        />
      </Grid>
    </Fragment>
  );
};
export default DhComponent;
