import { useState, MouseEvent, useEffect } from 'react';
import Router from 'next/router';

import * as AuthService from 'App/Helpers/Auth';

const Logic = () => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = async () => {
    await AuthService.logout();
    Router.replace('/');
  };

  const initFunction = async () => {
    // Init Client Side
  };

  useEffect(() => {
    initFunction();
  }, []);
  
  return {
    anchorEl,
    setAnchorEl,
    open,
    handleClick,
    handleClose,
    handleLogout,
  };
};

export default Logic;