/* eslint-disable import/no-anonymous-default-export */
import { getMenuPengaturans, TMenuPengaturans } from '@/Redux/Slices/MenusPengaturanSlice';
import { TMenus, getMenus } from '@/Redux/Slices/MenusSlice';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { IProps } from './Index';

export default (props: IProps) => {
  const [loading, setLoading] = useState(true);
  const menus: TMenus = useSelector(getMenus);
  const menuPengaturans: TMenuPengaturans = useSelector(getMenuPengaturans);
  const initFunction = async () => {
    // Init Client Side
  };
  useEffect(() => {
    initFunction();
  }, []);
  return {
    loading,
    menus,
    menuPengaturans,
  };
};
