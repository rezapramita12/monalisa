import { FC, ReactNode, Fragment, useState } from 'react';

import useLogic from './Logic';
import Header from '../Header';
import Sidebar from '../Sidebar';
import { AppBar, Box, Button, Collapse, CssBaseline, Divider, Drawer, List, ListItem, ListItemButton, ListItemIcon, ListItemText, ListSubheader, Toolbar, Typography } from '@mui/material';

import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import DhIcon from '@/Components/Icon';
import NextLink from '@/Components/Link/NextLink';
import { ExpandLess, ExpandMore } from '@mui/icons-material';
import DhButton from '@/Components/Button';

export interface IProps {
  children: ReactNode;

}

const AppLayout: FC<IProps> = (props: IProps) => {
  const { children } = props;
  const { loading } = useLogic(props);
  const drawerWidth = 240;
  const { menus, menuPengaturans } = useLogic(props);
  const [isOpen, setIsOpen] = useState(false);
  const [open, setOpen] = useState();
  const handleClick = (itemID: any, isOpen: boolean) => {
    var name;
    setIsOpen(!isOpen);
    if (isOpen) {
      setOpen(itemID);
    } else {
      setOpen(name);
    }
    // or simply setState({ openItemID: itemID });
  };
  return (
    <Fragment>
      <Box sx={{ display: 'flex' }}>
        <CssBaseline />
        <AppBar
          position="fixed"
          sx={{ width: `calc(100% - ${drawerWidth}px)`, ml: `${drawerWidth}px` }}
        >
          <Toolbar>
            <Typography variant="h6" noWrap component="div">
              Permanent drawer
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          sx={{
            width: drawerWidth,
            flexShrink: 0,
            '& .MuiDrawer-paper': {
              width: drawerWidth,
              boxSizing: 'border-box',
            },
          }}
          variant="permanent"
          anchor="left"
        >

          <Toolbar />
          <Box textAlign='center' >

            <Typography sx={{ fontSize: 20, fontWeight: 'bold' }} >Monalisa</Typography>
          </Box>
          <Divider />

          <Divider />
          <List>
            <ListSubheader>Main Menu</ListSubheader>

            {menus.map((result, index) => (
              <div key={result.id}>
                {result.childMenus != null ?
                  <div key={result.id}>
                    <ListItem className='root' button key={result.id} onClick={() => handleClick(result.id, isOpen)} >
                      <ListItemIcon>
                        <DhIcon name={result.icon ?? ''} href={result.link}></DhIcon>
                        {/* {index % 2 === 0 ? <InboxIcon /> : <MailIcon />} */}
                      </ListItemIcon>
                      <ListItemText primary={result.name} />
                      {open == result.id ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse key={result.id} component="li" in={open == result.id} timeout="auto" unmountOnExit>
                      <List disablePadding>
                        {result.childMenus?.map((sitem) => {
                          return (
                            <NextLink key={index} href={sitem.link ?? ''} noLinkStyle>

                              <ListItem button key={sitem.id} className='nested' >

                                <ListItemText key={sitem.id} primary={sitem.name} />
                              </ListItem>
                            </NextLink>
                          );
                        })}
                      </List>
                    </Collapse>
                  </div> : <ListItem button onClick={() => handleClick(result.name, isOpen)} key={result.id}> <ListItemIcon>
                    <DhIcon name={result.icon ?? ''} href={result.link}></DhIcon>
                    {/* {index % 2 === 0 ? <InboxIcon /> : <MailIcon />} */}
                  </ListItemIcon>
                    <ListItemText primary={result.name} />
                  </ListItem>}
              </div>
            )

            )}
          </List>
          <List>
            <ListSubheader>Pengaturan</ListSubheader>

            {menuPengaturans.map((result, index) => (
              <div key={result.id}>
                {result.childMenus != null ?
                  <div key={result.id}>
                    <ListItem className='root' button key={result.id} onClick={() => handleClick(result.id, isOpen)} >
                      <ListItemIcon>
                        <DhIcon name={result.icon ?? ''} href={result.link}></DhIcon>
                        {/* {index % 2 === 0 ? <InboxIcon /> : <MailIcon />} */}
                      </ListItemIcon>
                      <ListItemText primary={result.name} />
                      {open == result.id ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse key={result.id} component="li" in={open == result.id} timeout="auto" unmountOnExit>
                      <List disablePadding>
                        {result.childMenus?.map((sitem) => {
                          return (
                            <NextLink key={index} href={sitem.link ?? ''} noLinkStyle>

                              <ListItem button key={sitem.id} className='nested' >

                                <ListItemText key={sitem.id} primary={sitem.name} />
                              </ListItem>
                            </NextLink>
                          );
                        })}
                      </List>
                    </Collapse>
                  </div> : <ListItem button onClick={() => handleClick(result.name, isOpen)} key={result.id}> <ListItemIcon>
                    <DhIcon name={result.icon ?? ''} href={result.link}></DhIcon>
                    {/* {index % 2 === 0 ? <InboxIcon /> : <MailIcon />} */}
                  </ListItemIcon>
                    <ListItemText primary={result.name} />
                  </ListItem>}
              </div>
            )

            )}
          </List>
          <Box textAlign='center'>

            <Button
              fullWidth
              size="medium"
              variant="contained"
              color="primary"
              sx={{ width: '60%', marginTop: 3, fontSize: 12, textTransform: 'capitalize', justifyCOntent: 'center' }}
              type="submit"
            >
              Logout
            </Button>
          </Box>
        </Drawer>
        <Box
          component="main"
          sx={{ flexGrow: 1, bgcolor: 'background.default', p: 3 }}
        >
          <Toolbar />
          {children}
        </Box>
      </Box>
    </Fragment >
  );

};

export default AppLayout;


