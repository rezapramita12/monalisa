/* eslint-disable react/jsx-key */
import { Fragment, FC } from 'react';

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Container from '@mui/material/Container';
import { styled } from '@mui/material/styles';

import DhMenuDesktop from 'App/Layouts/Components/MenuDesktop/NavMenu/Index';
import DhMenuMobile from 'App/Layouts/Components/NavMenuMobile';
import DHIcon from 'App/Components/Icon';
import { Grid } from '@material-ui/core';

// TODO: sementara
export const menus = [
  {
    name: 'Dashboard',
    icon: <DHIcon name="menu-icon-dashboard" />,
  },
  {
    name: 'Mahasiswa',
    icon: <DHIcon name="menu-icon-registration" />,
  },
  {
    name: 'Dosen',
    icon: <DHIcon name="menu-icon-doctor" />,
  },
  {
    name: 'Perpus',
    icon: <DHIcon name="menu-icon-pharmacy" />,
  },
  {
    name: 'Keuangan',
    icon: <DHIcon name="menu-icon-cashier" />,
  },
  {
    name: 'Broadcast',
    icon: <DHIcon name="menu-icon-master" />,
  },
];

const StyledAppBar = styled(AppBar)(({ theme }) => ({
  backgroundColor: theme.palette.grey[50],
  height: '40px',
  boxShadow: '0px 2px 20px rgb(1 41 112 / 10%)',
}));

const Navbar: FC = () => {
  return (
    <Fragment>
      <StyledAppBar position="static" elevation={0}>
        <Container maxWidth={false}>
          <Toolbar disableGutters>
            <DhMenuMobile />
            <DhMenuDesktop />
          </Toolbar>
        </Container>
      </StyledAppBar>
    </Fragment>
  );
};

export default Navbar;
