import { Home } from '@mui/icons-material';
import { Typography } from '@mui/material';
import { Component } from 'react';
import { Menu, MenuItem, Sidebar, SubMenu, useProSidebar } from 'react-pro-sidebar';

const SideBar = () => {
    const { collapseSidebar } = useProSidebar();

    return (
        <div style={{ display: 'flex', height: '100%', }}>
            <Sidebar  >
                <Menu>
                    <MenuItem >Main Menu</MenuItem>
                    <MenuItem icon={<Home />}>Dashboard</MenuItem>
                    <SubMenu prefix={<span className="badge yellow">Siswa</span>} title="Components" icon={<Home />} >

                        <MenuItem>Component 1</MenuItem>
                        <MenuItem>Component 2</MenuItem>
                    </SubMenu>
                    <SubMenu title="Components" icon={<Home />} prefix={<span className="badge yellow">Guru</span>}>

                        <MenuItem>Component 1</MenuItem>
                        <MenuItem>Component 2</MenuItem>
                    </SubMenu>
                    <MenuItem icon={<Home />}>Perpus</MenuItem>
                    <MenuItem icon={<Home />}>Keuangan</MenuItem>
                    <MenuItem icon={<Home />}>Broadcast</MenuItem>
                    <MenuItem icon={<Home />}>Pesan</MenuItem>
                    <SubMenu icon={<Home />} prefix={<span className="badge yellow">Berita&Beasiswa</span>}>
                        <MenuItem>Component 1</MenuItem>
                        <MenuItem>Component 2</MenuItem>
                    </SubMenu>
                    <MenuItem >Pengaturan</MenuItem>
                    <MenuItem icon={<Home />}>Bantuan</MenuItem>
                    <MenuItem icon={<Home />}>Setting</MenuItem>
                </Menu>

            </Sidebar>

        </div>
    );
};

export default SideBar;
