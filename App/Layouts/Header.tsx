import { FC, Fragment } from 'react';

import Topbar from './Topbar';
import Navbar from './Navbar';
import Sidebar from './Sidebar';

const Header: FC = () => {
  return (
    <Fragment>
      <Topbar />
      {/* <Navbar /> */}
      <Sidebar />
    </Fragment>
  );
};

export default Header;
