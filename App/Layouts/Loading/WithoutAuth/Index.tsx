/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */

import RouterLink from 'next/link';
import * as Yup from 'yup';
import Container from '@mui/material/Container';
import { Icon } from '@iconify/react';
import {
  Alert,
  Card,
  Grid,
  IconButton,
  InputAdornment,
  Stack,
  TextField,
  Link,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import Skeleton from '@mui/material/Skeleton';

const WrapperForm = styled('div')(({ theme }) => ({
  padding: theme.spacing(12, 0),
  textAlign: 'center',
}));

const StyledCard = styled(Card)(({ theme }) => ({
  boxShadow: '4px 4px 12px 3px rgb(45 156 219 / 20%)',
  borderRadius: 8,
  paddingLeft: 50,
  paddingRight: 50,
  paddingTop: 50,
  minHeight: '425px',
}));

const CenteredImage = styled('div')(({ theme }) => ({
  textAlign: 'center',
  paddingBottom: 20,
}));

export default function Login() {
  return (
    <Container maxWidth="lg">
      <Grid container spacing={5}>
        <Grid item md={7} sm={12}>
          <img src={'/img/hero-image-login.svg'} />
        </Grid>
        <Grid item md={5} sm={12}>
          <WrapperForm>
            <StyledCard>
              <CenteredImage>
                <img src={'/img/logo-cliniqu.svg'} />
                <Skeleton height={100} />
                <Skeleton height={100} />
                <Skeleton height={50} />
              </CenteredImage>
            </StyledCard>
          </WrapperForm>
        </Grid>
      </Grid>
    </Container>
  );
}
