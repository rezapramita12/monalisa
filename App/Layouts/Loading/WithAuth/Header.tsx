import { FC, Fragment } from 'react';

import Topbar from './Topbar';
import Navbar from './Navbar';

const Header: FC = () => {
  return (
    <Fragment>
      <Topbar />
      <Navbar />
    </Fragment>
  );
};

export default Header;
