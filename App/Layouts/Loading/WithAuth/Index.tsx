import React, { FC, ReactNode, Fragment } from 'react';
import Header from './Header';

export interface IProps {}

const DHComponent: FC<IProps> = (props: IProps) => {
  return (
    <Fragment>
      <Header />
    </Fragment>
  );
};

export default DHComponent;
