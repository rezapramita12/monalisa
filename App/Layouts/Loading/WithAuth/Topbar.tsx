/* eslint-disable @next/next/no-img-element */
import { AppBar, Box, Toolbar, Typography, Container } from '@mui/material';
import Skeleton from '@mui/material/Skeleton';
import { styled } from '@mui/material/styles';

const StyledAppBar: any = styled(AppBar)(({ theme }) => ({
  height: '40px',
  margin: 0,
  padding: 0,
}));
const StyledToolbar = styled(Toolbar)(({ theme }) => ({
  height: '40px',
  margin: 0,
  padding: 0,
}));
const StyledContainer = styled(Container)(({ theme }) => ({
  height: '40px',
  margin: 0,
  padding: 0,
}));

const ResponsiveAppBar = () => {
  return (
    <StyledAppBar elevation={0} position="static">
      <StyledContainer disableGutters maxWidth={false}>
        <StyledToolbar>
          <Typography
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 2,
              marginLeft: '0',
              marginTop: '-25px',
              display: { xs: 'none', md: 'flex' },
              fontWeight: 700,
              color: 'inherit',
              textDecoration: 'none',
            }}
          >
            Klinik DHealth
          </Typography>
          <Typography
            noWrap
            component="a"
            href=""
            sx={{
              mr: 2,
              display: { xs: 'flex', md: 'none' },
              flexGrow: 1,
              fontFamily: 'monospace',
              fontWeight: 700,
              color: 'inherit',
              textDecoration: 'none',
            }}
          >
            Klinik DHealth
          </Typography>
          <Box sx={{ flexGrow: 2, display: { xs: 'none', md: 'flex' } }}>
            <Skeleton
              width={300}
              height={45}
              style={{
                marginRight: 25,
                marginTop: -30,
              }}
            />
          </Box>
          <Box sx={{ display: { xs: 'flex', md: 'none' } }}></Box>
          <Box
            sx={{
              marginTop: '-25px',
              flexGrow: 1,
              justifyContent: 'end',
              display: { xs: 'none', md: 'flex' },
            }}
          >
            <Skeleton
              width={300}
              height={45}
              style={{
                marginRight: 25,
              }}
            />
            <img
              src="/img/cliniqu-nav-biru.png"
              width="150px"
              alt=""
              style={{ marginBottom: '2px', marginRight: '-23px' }}
            />
          </Box>
        </StyledToolbar>
      </StyledContainer>
    </StyledAppBar>
  );
};
export default ResponsiveAppBar;
