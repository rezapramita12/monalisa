/* eslint-disable react/jsx-key */
import { Fragment, FC } from 'react';

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Container from '@mui/material/Container';
import { styled } from '@mui/material/styles';
import Skeleton from '@mui/material/Skeleton';

const StyledAppBar = styled(AppBar)(({ theme }) => ({
  backgroundColor: theme.palette.grey[50],
}));

const Navbar: FC = () => {
  return (
    <Fragment>
      <StyledAppBar position="static">
        <Container maxWidth={false}>
          <Toolbar disableGutters>
            <Skeleton width="100%" height={60} />
          </Toolbar>
        </Container>
      </StyledAppBar>
    </Fragment>
  );
};

export default Navbar;
