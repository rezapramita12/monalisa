import { FC, ReactNode, Fragment } from 'react';
import Header from './Header';

export interface IProps {
  children: ReactNode;
}

const AppLayout: FC<IProps> = (props: IProps) => {
  const { children } = props;
  return (
    <Fragment>
      <Header />
      {children}
    </Fragment>
  );
};

export default AppLayout;
