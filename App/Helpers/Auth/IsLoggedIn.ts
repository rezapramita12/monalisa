import Cookies, { SessionDataType } from 'App/Utils/Cookies';

export const isLoggedIn = () => {
  const accountStatus = Cookies.getAccountStatus();
  if (accountStatus == 'logged_in') {
    return true;
  }
  return false;
};
