/* eslint-disable import/no-anonymous-default-export */
import Cookies, { SessionDataType } from 'App/Utils/Cookies';
import { DataItemType } from 'App/Types';

import AuthService from 'App/Services/Auth';

type TLogin = {
  email?: string;
  password?: string;
};

export const login = async (data: TLogin) => {
  const responseData = await AuthService.login(data);
  const isError = responseData?.statusCode != 200;
  if (isError) return responseData;
  const newSession = {
    token_type: 'Bearer',
    expires_in: 1659597347,
    access_token: responseData?.data?.token,
    refresh_token: 'Refresh-Token-Dummy',
  };
  Cookies.setSessionData(newSession);
  Cookies.setAccountStatus('logged_in');
  return responseData;
};
