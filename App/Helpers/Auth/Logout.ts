import Cookies, { SessionDataType } from 'App/Utils/Cookies';

import AuthService from 'App/Services/Auth';

export const logout = async () => {
  // Di Bypass dulu (Di endpoint nya error)
  //   await AuthService.logout();
  Cookies.clearSessionData();
  Cookies.setAccountStatus('');
};
