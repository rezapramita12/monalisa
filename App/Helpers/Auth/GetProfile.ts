import Cookies, { SessionDataType } from 'App/Utils/Cookies';

export const getProfile = () => {
  const token = Cookies.getSessionData();
  const dataProfile = {
    username: 'Dummy',
    ...token,
  };
  return dataProfile;
};
