export const getNormalizeFiltersEqual = (paramFilters: Array<any>) => {
  let tempFilters = null;
  if (paramFilters?.length < 1) return [];
  paramFilters.map((item, index) => {
    const nameKey = `filter.${item.id}`;
    const value = `$eq:${item.value}`;
    tempFilters = {
      [nameKey]: value,
    };
  });
  return tempFilters;
};

export const getNormalizeSorting = (paramSortings: Array<any>) => {
  let tempSorting = null;
  if (paramSortings?.length < 1) return [];
  paramSortings.map((item, index) => {
    const nameKey = 'sortBy';
    let sortType = item?.desc ? 'DESC' : 'ASC';
    const value = `${item.id}:${sortType}`;
    tempSorting = {
      [nameKey]: value,
    };
  });
  return tempSorting;
};
