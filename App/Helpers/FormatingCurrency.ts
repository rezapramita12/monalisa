export const toRupiah = (value: number) => {
  let formatter = new Intl.NumberFormat('en-ID', {
    style: 'currency',
    currency: 'IDR',
  })
    .format(value)
    .replace(/[IDR]/gi, '')
    .replace(/(\.+\d{2})/, '')
    .trimLeft();
  return `Rp. ${formatter}`;
};
