import { Grid, IconButton, Typography } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

interface IProps {
  title: string;
  handleClose: any;
}

const ModalHeader = (props: IProps) => {
  const { title, handleClose } = props;
  return (
    <Grid
      container
      sx={{
        backgroundColor: '#fafafa',
        marginBottom: 1,
        borderBottom: '1px solid #dddddd',
        paddingBottom: 1,
        paddingTop: 1,
        paddingLeft: 2,
        paddingRight: 2,
        borderRadius: 1,
      }}
    >
      <Grid item xs={6}>
        <Typography
          id="parent-modal-title"
          style={{ fontWeight: 'bold', color: '#606060', marginTop: 8 }}
        >
          {title}
        </Typography>
      </Grid>
      <Grid item xs={6}>
        <Typography align="right" style={{ fontWeight: 'bold', color: '#606060' }}>
          <IconButton onClick={handleClose}>
            <CloseIcon></CloseIcon>
          </IconButton>
        </Typography>
      </Grid>
    </Grid>
  );
};

export default ModalHeader;
