import { Box, Button, Grid } from '@mui/material';
import { LoadingButton } from '@mui/lab';

export interface IProps {
  formik: any;
  handleCloseModal: any;
  handleSubmit: any;
}

const ModalFooter = (props: IProps) => {
  const { formik, handleCloseModal, handleSubmit } = props;
  return (
    <Grid item xs={12}>
      <div
        style={{
          marginTop: '25px',
          borderTop: '1px solid #dddddd',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
        }}
      >
        <Box
          display="flex"
          justifyContent="flex-end"
          alignItems="flex-end"
          sx={{ width: 200, marginTop: '15px' }}
        >
          <Button
            size="small"
            variant="outlined"
            sx={{ width: 150, marginRight: 2 }}
            onClick={handleCloseModal}
          >
            Batal
          </Button>
          <LoadingButton
            fullWidth
            size="small"
            type="submit"
            variant="contained"
            loading={formik.isSubmitting}
            onClick={handleSubmit}
          >
            Simpan
          </LoadingButton>
        </Box>
      </div>
    </Grid>
  );
};

export default ModalFooter;
