import { Fragment, ReactNode, FC } from 'react';

import { SxProps } from '@mui/system';
import { Card, CardContent, CardHeader, Divider } from '@mui/material';

interface IProps {
  icon?: string;
  title: ReactNode;
  subheader?: string;
  children: ReactNode;
  sx?: SxProps;
  action: ReactNode;
}

const DhCard: FC<IProps> = ({ icon, title, subheader, children, sx, action }) => {
  return (
    <Fragment>
      <Card variant="outlined" sx={sx}>
        <CardHeader
          sx={{
            paddingBottom: '10px',
            paddingTop: '10px',
          }}
          title={title}
          subheader={subheader}
          action={action}
        />
        <Divider />
        <CardContent>{children}</CardContent>
      </Card>
    </Fragment>
  );
};

export default DhCard;
