import {
  Card,
  Grid,
  Link,
  TableContainer,
  Table,
  TableRow,
  TableCell,
  TableHead,
  TableBody,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import RouterLink from 'next/link';

type Summary = {
  src: string;
  title: string;
  url: string;
  tableHeader: string[];
  tableContent: string[] | number[];
};

const DashboardWrapper = styled('div')(({ theme }) => ({
  paddingTop: 20,
  paddingLeft: 50,
  paddingRight: 50,
  paddingBottom: 50,
}));

const StyledTitle = styled('p')(({ theme }) => ({
  display: 'inline',
  fontSize: 18,
  verticalAlign: 'middle',
  fontWeight: 700,
}));

const CardBody = styled('div')(({ theme }) => ({
  paddingTop: 20,
  paddingBottom: 20,
}));

const StyledImg = styled('img')(({ theme }) => ({
  // height: '100%',
  width: '2rem',
}));

const TitleWrapper = styled('div')(({ theme }) => ({
  paddingLeft: 15,
  paddingRight: 15,
  paddingTop: 10,
  borderBottom: '1px solid #DDDDDD',
}));

const LihatWrapper = styled('div')(({ theme }) => ({
  float: 'right',
  fontSize: 15,
  verticalAlign: 'middle',
  lineHeight: 2,
}));

const StyledCell = styled(TableCell)(({ theme }) => ({
  borderBottom: 'unset',
}));

const StyledTableContainer = styled(TableContainer)(({ theme }) => ({
  // boxShadow: 'unset'
}));

const SummaryTableCard = ({ src, title, url, tableHeader, tableContent }: Summary) => {
  return (
    <Card>
      <TitleWrapper>
        <Grid container>
          <Grid item sm={1} xs={2}>
            <StyledImg src={src} />
          </Grid>
          <Grid item sm={7} xs={10}>
            <StyledTitle>{title}</StyledTitle>
          </Grid>
          <Grid item sm={4} xs={12}>
            <LihatWrapper>
              <Link component={RouterLink} variant="subtitle2" href={url}>
                Lihat Selengkapnya
              </Link>
            </LihatWrapper>
          </Grid>
        </Grid>
      </TitleWrapper>
      <CardBody sx={{ padding: 1 }}>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                {tableHeader.map((header, index) => (
                  <StyledCell key={header + '' + index}>{header}</StyledCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                {tableContent.map((item, index) => (
                  <StyledCell key={item + '' + index}>{item}</StyledCell>
                ))}
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </CardBody>
    </Card>
  );
};

export default SummaryTableCard;
