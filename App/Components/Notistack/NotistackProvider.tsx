/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
import { ReactNode } from 'react';

import { SnackbarProvider } from 'notistack';
import { useTheme } from '@mui/material/styles';
import { GlobalStyles } from '@mui/material';
import { styled } from '@mui/material/styles';

const StyledSnackbarProvider = styled(SnackbarProvider)(({ theme }) => ({
  '&.SnackbarItem-variantSuccess': {
    backgroundColor: '#e3effe',
    color: 'black',
    border: '2px solid #2f7fec',
  },
  '&.SnackbarItem-variantError': {
    backgroundColor: '#fee4e6',
    color: 'black',
    border: '2px solid #e42a34',
  },
  '&.SnackbarItem-variantWarning': {
    backgroundColor: '#feeee3',
    color: 'black',
    border: '2px solid #fb8238',
  },
}));

function SnackbarStyles() {
  const theme = useTheme();
  const isLight = theme.palette.mode === 'light';

  return (
    <GlobalStyles
      styles={{
        '#root': {
          '& .SnackbarContent-root': {
            width: '100%',
            padding: theme.spacing(1.5),
            margin: theme.spacing(0.25, 0),
            borderRadius: theme.shape.borderRadius,
          },
          '& .SnackbarItem-message': {
            padding: '0 !important',
            fontWeight: theme.typography.fontWeightMedium,
          },
          '& .SnackbarItem-action': {
            marginRight: 0,
            color: theme.palette.action.active,
            '& svg': { width: 20, height: 20 },
          },
        },
      }}
    />
  );
}

type SnackbarIconProps = {
  src: string;
};

function SnackbarIcon({ src }: SnackbarIconProps) {
  return <img src={src} style={{ marginRight: 10 }} />;
}

type NotistackProviderProps = {
  children: ReactNode;
};

export default function NotistackProvider({ children }: NotistackProviderProps) {
  return (
    <>
      <SnackbarStyles />

      <StyledSnackbarProvider
        dense
        maxSnack={2}
        // preventDuplicate
        autoHideDuration={3000}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        iconVariant={{
          success: <SnackbarIcon src={'/img/icons/check_circle.svg'} />,
          error: <SnackbarIcon src={'/img/icons/error.svg'} />,
          warning: <SnackbarIcon src={'/img/icons/warning.svg'} />,
        }}
      >
        {children}
      </StyledSnackbarProvider>
    </>
  );
}
