import React from 'react';
import { useSnackbar } from 'notistack';
import {
  Box,
  Button,
  FormControl,
  FormControlLabel,
  Grid,
  IconButton,
  InputAdornment,
  MenuItem,
  Modal,
  Radio,
  RadioGroup,
  TextField as MuiTextField,
  Skeleton,
  Typography,
  Autocomplete,
  Paper,
  colors,
} from '@mui/material';
import closeFill from '@iconify/icons-eva/close-fill';
import { Icon } from '@iconify/react';

const Hook = () => {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const renderSnackbarError = (message: string) => {
    enqueueSnackbar(message, {
      variant: 'error',
      preventDuplicate: true,
      action: key => (
        <Button size="small" onClick={() => closeSnackbar(key)}>
          <Icon color={colors.grey[900]} icon={closeFill} />
        </Button>
      ),
    });
  };

  const renderSnackbarSuccess = (message: string) => {
    enqueueSnackbar(message, {
      variant: 'success',
      action: key => (
        <Button size="small" onClick={() => closeSnackbar(key)}>
          <Icon color={colors.grey[900]} icon={closeFill} />
        </Button>
      ),
    });
  };

  const renderSnackbarWarning = (message: string) => {
    enqueueSnackbar(message, {
      variant: 'warning',
      action: key => (
        <Button size="small" onClick={() => closeSnackbar(key)}>
          <Icon color={colors.grey[900]} icon={closeFill} />
        </Button>
      ),
    });
  };

  return {
    renderSnackbarError,
    renderSnackbarSuccess,
    renderSnackbarWarning,
  };
};

export default Hook;
