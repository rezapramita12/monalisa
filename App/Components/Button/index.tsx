import { FC, Fragment, ReactNode } from 'react';

import { Button } from '@mui/joy';

interface IProps {
  children: ReactNode;
  size?: 'normal' | 'small';
}

const DhButton: FC<IProps> = ({ children, size }) => {
  return (
    <Fragment>
      <Button>{children}</Button>
    </Fragment>
  );
};

export default DhButton;
