import React, { FC } from 'react';
import {
  DataGrid,
  GridColDef,
  GridRenderCellParams,
  gridPageCountSelector,
  gridPageSelector,
  useGridApiContext,
  useGridSelector,
} from '@mui/x-data-grid';
import { Box, Button, Switch } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { MoreVert } from '@mui/icons-material';
import Pagination from '@mui/material/Pagination';
import PaginationItem from '@mui/material/PaginationItem';
import CustomPagination from '../Pagination/index';
import CustomPaginationActionsTable from '../Pagination/table';
import TestPage from '../Pagination';
const cols: GridColDef[] = [
  { field: 'no', headerName: 'No', width: 10 },
  {
    field: 'tindakan',
    headerName: '',
    width: 10,
    renderCell: (params: GridRenderCellParams<Date>) => <MoreVert></MoreVert>,
  },
  { field: 'kodeTindakan', headerName: 'Kode Tindakan', width: 150 },
  { field: 'namaTindakan', headerName: 'Nama Tindakan', width: 150 },
  { field: 'namaLain', headerName: 'Nama Lain', width: 150 },
  { field: 'namaKategori', headerName: 'Nama Kategori', width: 150 },
  { field: 'namaKelompok', headerName: 'Nama Kelompok', width: 150 },
  { field: 'kelas', headerName: 'Kelas', width: 100 },
  { field: 'caraBayar', headerName: 'Cara Bayar', width: 100 },
  { field: 'tarif', headerName: 'Tarif', width: 100 },
  {
    field: 'status',
    headerName: 'Status',
    width: 100,
    renderCell: (params: GridRenderCellParams<Date>) => (
      <Switch
        checked={true}
        // onChange={handleChange}
        inputProps={{ 'aria-label': 'controlled' }}
      />
    ),
  },
  { field: 'catatan', headerName: 'Catatan', width: 100 },
];
const rows = [
  {
    id: 1,
    no: 1,
    kodeTindakan: 'TN001',
    namaTindakan: 'Abdomen',
    namaLain: 'Abdomen',
    namaKategori: 'Other',
    namaKelompok: 'Doctor Producer',
    kelas: 'Rawat Jalan',
    caraBayar: 'Umum',
    tarif: '120.000',
    status: 'aktif',
    catatan: '',
  },
  {
    id: 2,
    no: 2,
    kodeTindakan: 'TN001',
    namaTindakan: 'Abdomen',
    namaLain: 'Abdomen',
    namaKategori: 'Other',
    namaKelompok: 'Doctor Producer',
    kelas: 'Rawat Jalan',
    caraBayar: 'Umum',
    tarif: '120.000',
    status: 'aktif',
    catatan: '',
  },
  {
    id: 3,
    no: 3,
    kodeTindakan: 'TN001',
    namaTindakan: 'Abdomen',
    namaLain: 'Abdomen',
    namaKategori: 'Other',
    namaKelompok: 'Doctor Producer',
    kelas: 'Rawat Jalan',
    caraBayar: 'Umum',
    tarif: '120.000',
    status: 'aktif',
    catatan: '',
  },
  {
    id: 4,
    no: 4,
    kodeTindakan: 'TN001',
    namaTindakan: 'Abdomen',
    namaLain: 'Abdomen',
    namaKategori: 'Other',
    namaKelompok: 'Doctor Producer',
    kelas: 'Rawat Jalan',
    caraBayar: 'Umum',
    tarif: '120.000',
    status: 'aktif',
    catatan: '',
  },
  {
    id: 5,
    no: 5,
    kodeTindakan: 'TN001',
    namaTindakan: 'Abdomen',
    namaLain: 'Abdomen',
    namaKategori: 'Other',
    namaKelompok: 'Doctor Producer',
    kelas: 'Rawat Jalan',
    caraBayar: 'Umum',
    tarif: '120.000',
    status: 'aktif',
    catatan: '',
  },
  {
    id: 6,
    no: 6,
    kodeTindakan: 'TN001',
    namaTindakan: 'Abdomen',
    namaLain: 'Abdomen',
    namaKategori: 'Other',
    namaKelompok: 'Doctor Producer',
    kelas: 'Rawat Jalan',
    caraBayar: 'Umum',
    tarif: '120.000',
    status: 'aktif',
    catatan: '',
  },
  {
    id: 7,
    no: 7,
    kodeTindakan: 'TN001',
    namaTindakan: 'Abdomen',
    namaLain: 'Abdomen',
    namaKategori: 'Other',
    namaKelompok: 'Doctor Producer',
    kelas: 'Rawat Jalan',
    caraBayar: 'Umum',
    tarif: '120.000',
    status: 'aktif',
    catatan: '',
  },
  {
    id: 8,
    no: 8,
    kodeTindakan: 'TN001',
    namaTindakan: 'Abdomen',
    namaLain: 'Abdomen',
    namaKategori: 'Other',
    namaKelompok: 'Doctor Producer',
    kelas: 'Rawat Jalan',
    caraBayar: 'Umum',
    tarif: '120.000',
    status: 'aktif',
    catatan: '',
  },
  {
    id: 9,
    no: 9,
    kodeTindakan: 'TN001',
    namaTindakan: 'Abdomen',
    namaLain: 'Abdomen',
    namaKategori: 'Other',
    namaKelompok: 'Doctor Producer',
    kelas: 'Rawat Jalan',
    caraBayar: 'Umum',
    tarif: '120.000',
    status: 'aktif',
    catatan: '',
  },
  {
    id: 10,
    no: 10,
    kodeTindakan: 'TN001',
    namaTindakan: 'Abdomen',
    namaLain: 'Abdomen',
    namaKategori: 'Other',
    namaKelompok: 'Doctor Producer',
    kelas: 'Rawat Jalan',
    caraBayar: 'Umum',
    tarif: '120.000',
    status: 'aktif',
    catatan: '',
  },
  {
    id: 11,
    no: 11,
    kodeTindakan: 'TN001',
    namaTindakan: 'Abdomen',
    namaLain: 'Abdomen',
    namaKategori: 'Other',
    namaKelompok: 'Doctor Producer',
    kelas: 'Rawat Jalan',
    caraBayar: 'Umum',
    tarif: '120.000',
    status: 'aktif',
    catatan: '',
  },
  {
    id: 12,
    no: 12,
    kodeTindakan: 'TN001',
    namaTindakan: 'Abdomen',
    namaLain: 'Abdomen',
    namaKategori: 'Other',
    namaKelompok: 'Doctor Producer',
    kelas: 'Rawat Jalan',
    caraBayar: 'Umum',
    tarif: '120.000',
    status: 'aktif',
    catatan: '',
  },
];
const pageSizes = [5, 10, 20];
const TableMasterTindakan: FC = () => {
  const [checked, setChecked] = React.useState(true);
  const [pageSize, setPageSize] = React.useState(5);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
  };

  return (
    <Box
      sx={{
        height: 400,
        width: '100%',
        '& .super-app-theme--header': {
          backgroundColor: 'rgba(255, 255, 255,255)',
        },
      }}
    >
      <DataGrid
        columns={cols}
        rows={rows}
        pageSize={pageSize}
        pagination={true}
        onPageSizeChange={newPageSize => setPageSize(newPageSize)}
        rowsPerPageOptions={[5, 10, 20]}
        components={{
          Pagination: CustomPagination,
        }}
      />
      <div style={{ marginTop: '10px' }}>
        {'Menampilkan: '}
        <select
          onChange={e => {
            setPageSize(parseInt(e.target.value));

            // pageCount=e.target.va
          }}
          value={pageSize}
        >
          {pageSizes.map(size => (
            <option key={size} value={size}>
              {size}
            </option>
          ))}
        </select>
        {' dari ' + rows.length + ' data'}
      </div>
    </Box>
  );
};

export default TableMasterTindakan;
