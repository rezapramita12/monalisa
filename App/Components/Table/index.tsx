import { FC, useState } from 'react';

import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { LinearProgress } from '@mui/material';

import CustomNoRowsOverlay from './CustomNoRowsOverlay';
import { colors } from '@material-ui/core';

interface IProps {
  loading: boolean;
  height: number;
  width: string;
  rowsPerPageOptions: number;
  checkboxSelection?: boolean;
  columns: any;
  rows: any;
  newPageSize: number;
  disableSelectionOnClick?: boolean;
  rowId: any;
  sortModel?: any;
}

const DhTable: FC<IProps> = ({
  height,
  width,
  checkboxSelection,
  columns,
  rows,
  loading,
  newPageSize,
  disableSelectionOnClick = false,
  rowId,
  sortModel,
}) => {
  const [pageSizeOption, setPageSizeOption] = useState(newPageSize);
  const tableColumns: GridColDef[] = columns;
  const RowsPerPageOptions = [10, 25, 50, 100];

  return (
    <div style={{ height: height, width: width }}>
      <DataGrid
        sx={{
          '.MuiDataGrid-columnHeaders': {
            backgroundColor: colors.blueGrey[900],
            color: 'white',
          },
          '.MuiDataGrid-columnSeparator': {
            visibility: 'hidden',
          },
          '.MuiDataGrid-cell': {
            wordWrap: 'break-word !important',
          },
        }}
        // autoHeight
        rows={rows}
        getRowHeight={() => 'auto'}
        columns={tableColumns}
        pageSize={pageSizeOption}
        onPageSizeChange={newPageSize => setPageSizeOption(newPageSize)}
        rowsPerPageOptions={RowsPerPageOptions}
        checkboxSelection={checkboxSelection}
        disableSelectionOnClick={disableSelectionOnClick}
        getRowId={rowId}
        sortModel={sortModel}
        // TODO: loading skeleton-nya belum
        // loading={loading}
        components={{
          LoadingOverlay: LinearProgress,
          NoRowsOverlay: CustomNoRowsOverlay,
          // Pagination: CustomPagination,
        }}
        loading={loading}
      />
    </div>
  );
};

export default DhTable;
