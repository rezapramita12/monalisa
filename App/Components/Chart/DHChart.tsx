import { useEffect } from 'react';

import { Chart } from 'chart.js';
import { Card } from '@mui/material';
import { styled } from '@mui/material/styles';
import Typography from '@mui/material/Typography';

declare global {
  interface Window {
    myLine?: any;
  }
}

type Chart = { title: string; id: string; config: {} };

const CardBody = styled('div')(() => ({
  padding: 20,
}));

const CanvasWrapper = styled('div')(() => ({
  height: '220px',
}));

const DhChart = ({ title, id, config }: Chart) => {
  useEffect(() => {
    const ctx = document?.getElementById(id) as HTMLCanvasElement | null;
    const context = ctx?.getContext('2d') as CanvasRenderingContext2D;
    window.myLine = new Chart(context, config);
  }, [config, id]);

  return (
    <Card>
      <CardBody>
        <Typography fontSize={18} fontWeight={700} sx={{ pb: 2 }}>
          {title}
        </Typography>
        <CanvasWrapper>
          <canvas style={{ width: '100%' }} id={id}></canvas>
        </CanvasWrapper>
      </CardBody>
    </Card>
  );
};

export default DhChart;
