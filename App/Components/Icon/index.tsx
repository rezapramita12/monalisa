import { FC } from 'react';

import SvgIcon, { SvgIconProps } from '@mui/material/SvgIcon';

import basicIcons from './icons.json';

type TIcon = {
  [name: string]: {
    path: string;
    viewBox?: string;
  };
};

interface IProps extends SvgIconProps {
  name: string;
}

const DhIcon: FC<IProps> = (props: IProps) => {
  const { name, ...svgProps } = props;
  const icons = basicIcons as TIcon;

  if (icons && typeof icons[name] !== 'undefined') {
    const { path, viewBox } = icons[name];

    return (
      <SvgIcon viewBox={viewBox} {...props}>
        <path d={path} />
      </SvgIcon>
    );
  }

  return null;
};

export default DhIcon;
