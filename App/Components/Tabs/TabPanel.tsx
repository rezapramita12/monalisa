import { ReactNode, Fragment } from 'react';

import { SxProps } from '@mui/system';
import { Box, Typography } from '@mui/material';

interface TabPanelProps {
  children?: ReactNode;
  index: number;
  value: number;
  sx?: SxProps;
}

const TabPanel = (props: TabPanelProps) => {
  const { children, value, index, sx, ...other } = props;

  return (
    <Fragment>
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`tabpanel-${index}`}
        aria-labelledby={`tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={sx}>
            <Typography component={'span'}>{children}</Typography>
          </Box>
        )}
      </div>
    </Fragment>
  );
};

export default TabPanel;
