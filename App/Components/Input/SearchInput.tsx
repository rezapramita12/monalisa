import { styled, alpha } from '@mui/material/styles';
import InputBase, { InputBaseComponentProps } from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';

type TProps = {
  key?: string | number;
  placeholder?: string;
  inputProps?: InputBaseComponentProps;
};

const DhComponent = (props: TProps) => {
  const { placeholder = 'Search', inputProps } = props;

  const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(5),
      width: 'auto',
    },
  }));

  const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }));

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        width: '120px',
        '&:focus': {
          width: '150px',
        },
      },
      [theme.breakpoints.up('lg')]: {
        width: '300px',
        '&:focus': {
          width: '500px',
        },
      },
    },
  }));

  return (
    <Search>
      <SearchIconWrapper>
        <SearchIcon />
      </SearchIconWrapper>
      <StyledInputBase
        placeholder={placeholder}
        inputProps={{ 'aria-label': 'search', ...inputProps }}
      />
    </Search>
  );
};
export default DhComponent;
