import { TextField, TextFieldTypeMap, InputProps } from '@mui/joy';
import { OverridableComponent } from '@mui/types';
import { styled } from '@mui/material/styles';

const TextFieldStyled = styled(TextField)(({ theme }) => ({
  marginBottom: theme.spacing(1.5),
  input: {
    '&::placeholder': {
      fontSize: 12,
    },
  },
}));

const CustomTextField = (props: any) => {
  return <TextFieldStyled {...props} />;
};

export default CustomTextField;
