import React, { useState } from 'react';
import NumberFormat from 'react-number-format';

const CurrencyFieldText = ({ ...props }) => {
  const [displayValue, setDisplayValue] = useState<any>();

  return (
    <NumberFormat
      customInput={props.customInput}
      min={0}
      isNumericString={true}
      thousandSeparator={true}
      value={displayValue}
      decimalScale={2}
      onValueChange={(vals: any) => {
        setDisplayValue({ value: vals.formattedValue });
      }}
      {...props}
    />
  );
};

export default CurrencyFieldText;
