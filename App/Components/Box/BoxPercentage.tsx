import Typography from '@mui/material/Typography';
import { Grid } from '@mui/material';
import { styled } from '@mui/material/styles';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';

export interface BoxPercentage {
  src: string;
  title: string;
  amount: number;
  percentage: number;
  isUp: boolean;
}

const IconDashboard = styled('img')(({ theme }) => ({
  width: 50,
}));

const RingkasanBox = styled('div')(({ theme }) => ({
  padding: 20,
  border: '1px solid rgba(0, 0, 0, 0.125)',
  borderRadius: '0.25rem',
}));

const StyledTypography = styled(Typography)(({ theme }) => ({
  fontSize: 14,
  //   whiteSpace: 'nowrap',
  // overflow: 'hidden',
  // textOverflow: 'ellipsis'
}));

const BoxPercentage = ({ src, title, amount, percentage, isUp }: BoxPercentage) => {
  const color = isUp == true ? '#34BFA3' : '#E52A34';
  // if (percentage && percentage > 100) percentage = 100
  return (
    <RingkasanBox>
      <Grid container>
        <Grid item xs={4}>
          <IconDashboard src={src} />
        </Grid>
        <Grid item xs={8}>
          <StyledTypography fontSize={14}>{title}</StyledTypography>
          <Grid container>
            <Grid item xs={6}>
              <StyledTypography sx={{ fontSize: 14, wordWrap: 'break-word' }} fontWeight={700}>
                {amount}
              </StyledTypography>
            </Grid>
            <Grid item xs={6}>
              <StyledTypography sx={{ fontSize: 14, color: color, wordWrap: 'break-word' }}>
                {isUp ? (
                  <ArrowUpwardIcon sx={{ fontSize: 'unset' }} />
                ) : (
                  <ArrowDownwardIcon sx={{ fontSize: 'unset' }} />
                )}
                {percentage} %
              </StyledTypography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </RingkasanBox>
  );
};

export default BoxPercentage;
