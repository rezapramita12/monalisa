import Typography from '@mui/material/Typography';
import { Card, Grid } from '@mui/material';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';

type BoxWithoutPercentage = { src: string; title: string; amount: number };
const DashboardWrapper = styled('div')(({ theme }) => ({
  paddingTop: 20,
  paddingLeft: 50,
  paddingRight: 50,
  paddingBottom: 50,
}));

const IconDashboard = styled('img')(({ theme }) => ({
  width: 60,
}));

const RingkasanBox = styled(Box)(({ theme }) => ({
  padding: 20,
  border: '1px solid rgba(0, 0, 0, 0.125)',
  borderRadius: '0.25rem',
}));

const BoxWithoutPercentage = ({ src, title, amount }: BoxWithoutPercentage) => {
  return (
    <RingkasanBox>
      <Grid container rowSpacing={5}>
        <Grid item xs={4}>
          <IconDashboard src={src} />
        </Grid>
        <Grid item xs={8}>
          <Grid container rowSpacing={1}>
            <Grid item xs={12}>
              {title}
            </Grid>
            <Grid item xs={12}>
              <Typography fontWeight={700}>{amount}</Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </RingkasanBox>
  );
};

export default BoxWithoutPercentage;
