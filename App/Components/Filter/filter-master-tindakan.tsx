import { Refresh } from '@mui/icons-material';
import { Button, FormControl, Grid, InputLabel, MenuItem, Select, TextField } from '@mui/material';
import Router from 'next/router';
import { useRouter } from 'next/router';

import { Formik, Field, Form, useFormik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';

import KelompokTindakanService from 'App/Services/Master/KelompokTindakanService';
import { setListKelompokTindakan } from 'App/Redux/Slices/KelompokTindakanSlice';
import TindakanService from 'App/Services/Master/TindakanService';
import { setListTindakan } from 'App/Redux/Slices/TindakanSlice';
const FilterMasterTindakan = () => {
  const router = useRouter();
  const dispatch = useDispatch();

  const handleRefresh = () => {
    router.reload();
  };

  const formik = useFormik({
    initialValues: {
      serviceGroup: '',
      status: '',
    },
    onSubmit: async values => {
      const { serviceGroup, status } = values;
      const result = await TindakanService.getAll(serviceGroup, status);
      const resultData = result?.data;

      let i = 1;
      const resultMap = resultData.map((item: any, index: any) => {
        const number = index + 1;
        return {
          ...item,
          num_row: number,
        };
      });
      dispatch(setListTindakan(resultMap));
    },
  });
  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container columnSpacing={{ xs: 1, sm: 2, md: 2 }} sx={{ marginBottom: 2 }}>
        <Grid item xs={3}>
          <TextField
            fullWidth
            id="outlined-basic"
            inputProps={{ style: { height: 25 } }}
            label="Kode Tindakan / Nama Tindakan"
            variant="outlined"
            size="small"
            autoComplete="serviceGroup"
            name="serviceGroup"
            type="text"
            value={formik.values.serviceGroup}
            onChange={formik.handleChange}
          />
        </Grid>
        <Grid item xs={2}>
          <FormControl
            fullWidth
            sx={{
              width: '100%',
              height: 35,
            }}
            size="small"
          >
            <InputLabel id="demo-simple-select-label">Pilih Status</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              label="Pilih Status"
              name="status"
              defaultValue={''}
              value={formik.values.status}
              onChange={formik.handleChange}
            >
              <MenuItem value="">Pilih Status</MenuItem>
              <MenuItem value="1">Aktif</MenuItem>
              <MenuItem value="0">Tidak Aktif</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={1}>
          <Button
            fullWidth
            variant="outlined"
            color="primary"
            sx={{ height: 40, width: '100%' }}
            type="submit"
          >
            Cari
          </Button>
        </Grid>
        <Grid item xs={1}>
          <Button
            fullWidth
            variant="outlined"
            color="primary"
            sx={{ height: 40 }}
            onClick={() => {
              Router.reload();
            }}
          >
            <Refresh />
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default FilterMasterTindakan;
