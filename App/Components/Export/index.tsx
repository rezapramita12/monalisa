import { Fragment, FC } from 'react';

import * as XLSX from 'xlsx';
import { Button } from '@mui/material';
import Excel from 'exceljs';
import DhIcon from 'App/Components/Icon';

interface IProps {
  data: any;
  heading: any;
  fileName: string;
  tindakan: string;
  status: string;
}

const DhExport: FC<IProps> = ({ data, heading, fileName, tindakan, status }) => {
  const downloadExcel = (data: any) => {
    // const workbook = new Excel.Workbook();
    // const worksheet = workbook.addWorksheet("TOTAL COGS");
    // worksheet.getCell("A1").value = 'company';
    // worksheet.getCell("A2").value = 'TOTAL COGS';
    // workbook.xlsx.writeFile('reza.xlsx');
    const wb = XLSX.utils.book_new();
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet([]);

    // modify value if D4 is undefined / does not exists
    XLSX.utils.sheet_add_aoa(ws, [['Master Tindakan']], { origin: 'E1', cellStyles: true });
    XLSX.utils.sheet_add_aoa(ws, [['Kode Tindakan/Nama Tindakan : ' + tindakan]], { origin: 'A3' });
    XLSX.utils.sheet_add_aoa(ws, [['Status: ' + status]], { origin: 'A4' });
    XLSX.utils.sheet_add_aoa(ws, heading, { origin: 'A7' });
    XLSX.utils.sheet_add_json(ws, data, { origin: 'A8', skipHeader: true });
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, fileName + '.xlsx');
  };

  return (
    <Fragment>
      <Button
        sx={{ '&.MuiButtonBase-root:hover': { bgcolor: 'transparent' } }}
        onClick={() => downloadExcel(data)}
      >
        <DhIcon name="menu-icon-excel" />
      </Button>
    </Fragment>
  );
};

export default DhExport;
