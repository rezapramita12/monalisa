import { Grid } from '@mui/material';
import React from 'react';
import {
  gridPageCountSelector,
  gridPageSelector,
  gridPageSizeSelector,
  useGridApiContext,
  useGridSelector,
} from '@mui/x-data-grid';
import Pagination from '@mui/material/Pagination';
import PaginationItem from '@mui/material/PaginationItem';

type Props = {
  pageSize: number;
  setPageSize: React.Dispatch<React.SetStateAction<number>>;

  // password: string;
  // afterSubmit?: string;
};

const pageSizes = [10, 30, 50];
const CustomPagination: React.FC<Props> = ({ pageSize, setPageSize }: Props) => {
  const apiRef = useGridApiContext();
  const page = useGridSelector(apiRef, gridPageSelector);
  const pageCount = useGridSelector(apiRef, gridPageCountSelector);
  return (
    <Grid>
      {/* <Grid item xs={9}>
                <div style={{ marginTop: '10px', marginLeft: '10px' }}>
                    {"Menampilkan: "}
                    <select onChange={(e) => {
                        setPageSize(parseInt(e.target.value));

                        // pageCount=e.target.va
                    }} value={pageSize}>
                        {pageSizes.map((size) => (
                            <option key={size} value={size}>
                                {size}
                            </option>
                        ))}
                    </select> */}
      {/* {" dari " + rows.length + " data"} */}
      {/* </div> */}
      {/* </Grid> */}
      <Grid item xs={3} style={{ textAlign: 'right' }}>
        <Pagination
          color="primary"
          variant="outlined"
          shape="rounded"
          page={page + 1}
          count={pageCount}
          // @ts-expect-error
          renderItem={props2 => <PaginationItem {...props2} disableRipple />}
          onChange={(event: React.ChangeEvent<unknown>, value: number) =>
            apiRef.current.setPage(value - 1)
          }
        />
      </Grid>
    </Grid>
  );
};

export default CustomPagination;
