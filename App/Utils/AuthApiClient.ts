/* eslint-disable import/no-anonymous-default-export */
import axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';
import * as rax from 'retry-axios';
import { NextPageContext } from 'next';

import getConfig from 'next/config';

import Cookies, { SessionDataType } from 'App/Utils/Cookies';
import { DataItemType } from 'App/Types';

const { publicRuntimeConfig: Config } = getConfig();

export type ClientResponse = AxiosResponse<DataItemType>;
export type ErrorResponse = Error | AxiosError;

export interface ClientInstance {
  get: (url: string, params?: DataItemType, config?: AxiosRequestConfig) => Promise<ClientResponse>;
  post: (url: string, data?: DataItemType, config?: AxiosRequestConfig) => Promise<ClientResponse>;
  put: (url: string, data?: DataItemType, config?: AxiosRequestConfig) => Promise<ClientResponse>;
  patch: (url: string, data?: DataItemType, config?: AxiosRequestConfig) => Promise<ClientResponse>;
  delete: (url: string, config?: AxiosRequestConfig) => Promise<ClientResponse>;
}

const setToken = (session: SessionDataType) => `${session?.tokenType} ${session?.accessToken}`;

const getToken = async (context?: NextPageContext) => {
  const session = Cookies.getSessionData(context);
  const token = session ? setToken(session) : null;
  const refreshToken = session ? session.refreshToken : null;
  return { token, refreshToken };
};

const Client = axios.create({
  baseURL: Config.AUTH_API_URL,
  headers: {
    'Content-Type': 'application/json',
    'X-Tenant-Domain': 'a.cliniqu.id', // Dummy Cliniq Data
  },
  // timeout: 5000,
});

const handleRefreshToken = async (
  e: ErrorResponse,
  callback: (result?: AxiosResponse<DataItemType>, error?: ErrorResponse) => void,
) => {
  const { refreshToken } = await getToken();
  // const cfg = rax.getConfig(e);
  // console.log(`Retry attempt #${cfg.currentRetryAttempt}`);
  if (axios.isAxiosError(e)) {
    if ([401].includes(e?.response?.status as number) && refreshToken) {
      return Client.post('/clients/web/admin/refresh', {
        refresh_token: refreshToken,
      })
        .then(async result => {
          e.config.headers.Authorization = setToken(result?.data);
          await Cookies.setSessionData(result?.data);
          callback();
        })
        .catch(error => callback(error));
    }
  }

  return callback();
};

Client.defaults.raxConfig = {
  instance: Client,
  statusCodesToRetry: [
    [100, 199],
    [401, 401],
    [429, 429],
    [500, 599],
  ],
  onRetryAttempt: e =>
    new Promise((resolve, reject) => {
      handleRefreshToken(e, (result, error) => {
        if (!error) {
          resolve(result);
        } else {
          reject(error);
        }
      });
    }),
};

rax.attach(Client);

const getIntance = (
  url: string,
  params?: DataItemType,
  config?: AxiosRequestConfig,
): Promise<ClientResponse> =>
  Client.get(url, {
    params,
    ...config,
  });

const postIntance = (
  url: string,
  data?: DataItemType,
  config?: AxiosRequestConfig,
): Promise<ClientResponse> =>
  Client.post(url, data, {
    ...config,
  });

const putIntance = (
  url: string,
  data?: DataItemType,
  config?: AxiosRequestConfig,
): Promise<ClientResponse> =>
  Client.put(url, data, {
    ...config,
  });

const patchIntance = (
  url: string,
  data?: DataItemType,
  config?: AxiosRequestConfig,
): Promise<ClientResponse> =>
  Client.patch(url, data, {
    ...config,
  });

const deleteIntance = (url: string, config?: AxiosRequestConfig): Promise<ClientResponse> =>
  Client.delete(url, {
    ...config,
  });

const withAuth = async (): Promise<ClientInstance> => {
  const { token } = await getToken();
  const setConfig = (config?: AxiosRequestConfig) => ({
    ...config,
    headers: {
      ...config?.headers,
      Authorization: token,
    },
  });

  return {
    get: (url: string, params?: DataItemType, config?: AxiosRequestConfig) =>
      getIntance(url, params, setConfig(config)),
    post: (url: string, data?: DataItemType, config?: AxiosRequestConfig) =>
      postIntance(url, data, setConfig(config)),
    put: (url: string, data?: DataItemType, config?: AxiosRequestConfig) =>
      putIntance(url, data, setConfig(config)),
    patch: (url: string, data?: DataItemType, config?: AxiosRequestConfig) =>
      patchIntance(url, data, setConfig(config)),
    delete: (url: string, config?: AxiosRequestConfig) => deleteIntance(url, setConfig(config)),
  };
};

export default {
  get: getIntance,
  post: postIntance,
  put: putIntance,
  patch: patchIntance,
  delete: deleteIntance,
  withAuth,
};
