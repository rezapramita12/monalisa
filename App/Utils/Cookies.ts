/* eslint-disable import/no-anonymous-default-export */
import nookies from 'nookies';
import { NextPageContext } from 'next';
import getConfig from 'next/config';

import { DataItemType } from 'App/Types';

const { publicRuntimeConfig: Config } = getConfig();

export interface SessionDataType {
  accessToken: string;
  refreshToken: string;
  tokenType: string;
  expiresIn: string;
  lastUpdate: string;
}

// Note: context or context is required when you call this function on SSR
const options = { path: '/', domain: Config.COOKIES_DOMAIN };
const cookiesPrefix = Config.APP_ENVIRONMENT !== 'production' ? Config.APP_ENVIRONMENT : '';

const getCookiesName = (name: string): string => `${cookiesPrefix}_env_${name}`;

const getSessionData = (context?: NextPageContext): SessionDataType => {
  const cookies = nookies.get(context || null, options);
  let accessToken = cookies?.[getCookiesName('accessToken')];
  let refreshToken = cookies?.[getCookiesName('refreshToken')];

  // any cookie title with 'accessTokenExt-' prefix will be concatenated to the accessToken
  Object.keys(cookies).forEach(title => {
    if (title.includes(getCookiesName('accessTokenExt'))) accessToken += cookies?.[title] || '';
  });

  Object.keys(cookies).forEach(title => {
    if (title.includes(getCookiesName('refreshTokenExt'))) refreshToken += cookies?.[title] || '';
  });

  return {
    accessToken,
    refreshToken,
    tokenType: cookies?.[getCookiesName('tokenType')],
    expiresIn: cookies?.[getCookiesName('expiresIn')],
    lastUpdate: cookies?.[getCookiesName('lastUpdate')],
  };
};

const setSessionData = (cookies: DataItemType, context?: NextPageContext): void => {
  // split accessToken foreach 3500 char length
  const accessTokens = cookies?.access_token?.match(/.{1,3500}/g);

  accessTokens.forEach((tokenPart: string, i: number) => {
    const title = i === 0 ? getCookiesName('accessToken') : getCookiesName(`accessTokenExt-${i}`);
    nookies.set(context || null, title, tokenPart, options);
  });

  const refreshToken = cookies?.refresh_token?.match(/.{1,3500}/g);

  refreshToken.forEach((tokenPart: string, i: number) => {
    const title = i === 0 ? getCookiesName('refreshToken') : getCookiesName(`refreshTokenExt-${i}`);
    nookies.set(context || null, title, tokenPart, options);
  });

  nookies.set(context || null, getCookiesName('tokenType'), cookies?.token_type, options);
  nookies.set(context || null, getCookiesName('expiresIn'), cookies?.expires_in, options);
  nookies.set(context || null, getCookiesName('lastUpdate'), Date.now().toString(), options);
};

const getAccountStatus = (context?: NextPageContext): string => {
  const cookies = nookies.get(context || null, options);
  return cookies?.[getCookiesName('accountStatus')];
};

const setAccountStatus = (status: string, context?: NextPageContext): void => {
  nookies.set(context || null, getCookiesName('accountStatus'), status, options);
};

const clearSessionData = async (context?: NextPageContext): Promise<void> => {
  const cookies = nookies.get(context || null, options);
  const accessToken = Object.keys(cookies).filter(title => {
    const extTitle = getCookiesName(title);
    return extTitle.includes(getCookiesName('accessTokenExt'));
  });

  const refreshToken = Object.keys(cookies).filter(title => {
    const extTitle = getCookiesName(title);
    return extTitle.includes(getCookiesName('refreshTokenExt'));
  });

  await Promise.resolve();
  // Cookie Not Being Destroyed, https://github.com/maticzav/nookies/issues/365
  accessToken.forEach(v => {
    nookies.destroy(context || null, v, options);
  });

  refreshToken.forEach(v => {
    nookies.destroy(context || null, v, options);
  });

  nookies.destroy(context || null, getCookiesName('accessToken'), options);
  nookies.destroy(context || null, getCookiesName('refreshToken'), options);
  nookies.destroy(context || null, getCookiesName('expiresIn'), options);
  nookies.destroy(context || null, getCookiesName('tokenType'), options);
  nookies.destroy(context || null, getCookiesName('lastUpdate'), options);
  nookies.destroy(context || null, getCookiesName('accountStatus'), options);
};

export default {
  getCookiesName,
  getSessionData,
  setSessionData,
  getAccountStatus,
  setAccountStatus,
  clearSessionData,
};
