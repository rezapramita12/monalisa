/* eslint-disable import/no-anonymous-default-export */
import fileDownload from 'js-file-download';

import { DataItemType } from 'App/Types';
import MasterApiClient from 'App/Utils/MasterApiClient';

const getOneObatAlkes = (id: number | string): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(`/medicine/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response),
  );
const postObatAlkes = (data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .post('/medicine', data)
      .then(result => result?.data)
      .catch(error => error?.response?.data),
  );
const updateObatAlkes = (id: number | string, data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/medicine/${id}`, data)
      .then(result => result?.data)
      .catch(error => error?.response?.data),
  );
const deleteObatAlkes = (id: number | string): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .delete(`/medicine/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response?.data),
  );

const getPagination = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/medicine/paginate', { ...q })
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const exportExcel = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(
        '/medicine/export-excel',
        { ...q },
        {
          responseType: 'blob',
        },
      )
      .then(result => {
        fileDownload(result?.data as any, 'Obat Alkes.xlsx');
      })
      .catch(error => error?.response),
  );

const getAll = (q?: any, status?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/medicine', { q: q, status: status })
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const update = (id: number, data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/medicine/${id}`, data)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const updateStatus = (id: number): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/medicine/update-status/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const store = (data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .post('/medicine', data, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

export default {
  getOneObatAlkes,
  postObatAlkes,
  updateObatAlkes,
  deleteObatAlkes,
  getAll,
  getPagination,
  exportExcel,
  update,
  updateStatus,
  store,
};
