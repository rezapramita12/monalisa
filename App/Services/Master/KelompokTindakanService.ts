/* eslint-disable import/no-anonymous-default-export */
import fileDownload from 'js-file-download';

import { DataItemType } from 'App/Types';
import MasterApiClient from 'App/Utils/MasterApiClient';

const getPagination = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/service-group/paginate', { ...q })
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const getOne = (id: number | string): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(`/service-group/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const getAll = (q?: any, status?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/service-group', { q: q, status: status })
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const exportExcel = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/service-group/export-excel', q, {
        responseType: 'blob',
      })
      .then(result => {
        fileDownload(result?.data as any, 'Kelompok Tindakan.xlsx');
      })
      .catch(error => error?.response),
  );

const update = (id: number, data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/service-group/${id}`, data)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const updateStatus = (id: number): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/service-group/update-status/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const store = (data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .post('/service-group', data)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

export default {
  getOne,
  getAll,
  exportExcel,
  update,
  updateStatus,
  store,
  getPagination,
};
