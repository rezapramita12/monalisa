/* eslint-disable import/no-anonymous-default-export */
import fileDownload from 'js-file-download';

import { DataItemType } from 'App/Types';
import MasterApiClient from 'App/Utils/MasterApiClient';

const getOne = (id: number | string): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(`/doctor-schedule/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response),
  );
const getSpecialist = (id: number | string): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(`/doctor-specialist?doctor_id=${id}`)
      .then(result => result?.data)
      .catch(error => error?.response),
  );
const getDoctorSpecialist = (): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/doctor-specialist')
      .then(result => result?.data)
      .catch(error => error?.response),
  );
const postDoctorSchedule = (data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .post('/doctor-schedule', data)
      .then(result => result?.data)
      .catch(error => error?.response?.data),
  );
const updateDoctorSchedule = (id: number | string, data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/doctor-schedule/${id}`, data)
      .then(result => result?.data)
      .catch(error => error?.response?.data),
  );
const deleteDoctorSchedule = (id: number | string): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .delete(`/doctor-schedule/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response?.data),
  );

const getPagination = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/doctor-schedule/paginate', { ...q })
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const exportExcel = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(
        '/doctor-schedule/export-excel',
        { ...q },
        {
          responseType: 'blob',
        },
      )
      .then(result => {
        fileDownload(result?.data as any, 'Jadwal Dokter.xlsx');
      })
      .catch(error => error?.response),
  );

const getAll = (q?: any, status?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/doctor-schedule', { q: q, status: status })
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const update = (id: number, data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/doctor-schedule/${id}`, data)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const updateStatus = (id: number): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/doctor-schedule/update-status/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const store = (data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .post('/doctor-schedule', data, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

export default {
  getOne,
  getSpecialist,
  getDoctorSpecialist,
  postDoctorSchedule,
  updateDoctorSchedule,
  deleteDoctorSchedule,
  getAll,
  getPagination,
  exportExcel,
  update,
  updateStatus,
  store,
};
