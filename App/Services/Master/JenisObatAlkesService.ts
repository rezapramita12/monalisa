/* eslint-disable import/no-anonymous-default-export */
import fileDownload from 'js-file-download';

import { DataItemType } from 'App/Types';
import MasterApiClient from 'App/Utils/MasterApiClient';

const getOne = (id: number | string): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(`/medicine-type/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const postMedicineType = (data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .post('/medicine-type', data)
      .then(result => result?.data)
      .catch(error => error?.response?.data),
  );
const updateMedicineType = (id: number | string, data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/medicine-type/${id}`, data)
      .then(result => result?.data)
      .catch(error => error?.response?.data),
  );
const deleteMedicineType = (id: number | string): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .delete(`/medicine-type/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response?.data),
  );

const getPagination = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/medicine-type/paginate', { ...q })
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const exportExcel = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(
        '/medicine-type/export-excel',
        { ...q },
        {
          responseType: 'blob',
        },
      )
      .then(result => {
        fileDownload(result?.data as any, 'Jenis Obat Alkes.xlsx');
      })
      .catch(error => error?.response),
  );

const getAll = (q?: any, status?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/medicine-type', { q: q, status: status })
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const update = (id: number, data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/medicine-type/${id}`, data)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const updateStatus = (id: number): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/medicine-type/update-status/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const store = (data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .post('/medicine-type', data, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

export default {
  getOne,
  postMedicineType,
  updateMedicineType,
  deleteMedicineType,
  getAll,
  getPagination,
  exportExcel,
  update,
  updateStatus,
  store,
};
