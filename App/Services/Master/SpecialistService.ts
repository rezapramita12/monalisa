/* eslint-disable import/no-anonymous-default-export */
import fileDownload from 'js-file-download';

import { DataItemType } from 'App/Types';
import MasterApiClient from 'App/Utils/MasterApiClient';

const getAll = (): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/specialist')
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const getOne = (id: number | string): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(`/specialist/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const getPagination = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/specialist/paginate', { ...q })
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const exportExcel = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(
        '/specialist/export-excel',
        { ...q },
        {
          responseType: 'blob',
        },
      )
      .then(result => {
        fileDownload(result?.data as any, 'Master Layanan.xlsx');
      })
      .catch(error => error?.response),
  );

const update = (id: number, data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/specialist/${id}`, data)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const updateStatus = (id: number): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/specialist/update-status/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const store = (data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .post('/specialist/create', data)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

export default {
  getOne,
  getAll,
  getPagination,
  exportExcel,
  update,
  updateStatus,
  store,
};
