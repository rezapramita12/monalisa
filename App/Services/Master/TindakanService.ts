/* eslint-disable import/no-anonymous-default-export */
import fileDownload from 'js-file-download';

import { DataItemType } from 'App/Types';
import MasterApiClient from 'App/Utils/MasterApiClient';

const getOne = (id: number | string): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(`/service/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const getPagination = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/service/paginate', { ...q })
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const exportExcel = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(
        '/service/export-excel',
        { ...q },
        {
          responseType: 'blob',
        },
      )
      .then(result => {
        fileDownload(result?.data as any, 'Master Tindakan.xlsx');
      })
      .catch(error => error?.response),
  );

const getAll = (q?: any, status?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/service', { q: q, status: status })
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const update = (id: number, data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/service/${id}`, data)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const updateStatus = (id: number): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/service/update-status/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const store = (data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .post('/service/create', data)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const createAttributes = (): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/service/create-attributes')
      .then(result => result?.data)
      .catch(error => error?.response),
  );

export default {
  getOne,
  getAll,
  getPagination,
  exportExcel,
  update,
  updateStatus,
  createAttributes,
  store,
};
