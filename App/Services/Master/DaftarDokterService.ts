/* eslint-disable import/no-anonymous-default-export */
import fileDownload from 'js-file-download';

import { DataItemType } from 'App/Types';
import MasterApiClient from 'App/Utils/MasterApiClient';

const getOne = (id: number | string): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(`/doctor/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const getPagination = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/doctor/paginate', { ...q })
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const exportExcel = (q?: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get(
        '/doctor/export-excel',
        { ...q },
        {
          responseType: 'blob',
        },
      )
      .then(result => {
        fileDownload(result?.data as any, 'Daftar Dokter.xlsx');
      })
      .catch(error => error?.response),
  );

const getAll = (): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .get('/doctor')
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const update = (id: number, data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/doctor/${id}`, data, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const updateStatus = (id: number): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .patch(`/doctor/update-status/${id}`)
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

const store = (data: any): Promise<DataItemType> =>
  MasterApiClient.withAuth().then(api =>
    api
      .post('/doctor', data, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
      .then(result => result?.data)
      .catch(error => error?.response.data),
  );

export default {
  getOne,
  getAll,
  getPagination,
  exportExcel,
  update,
  updateStatus,
  store,
};
