/* eslint-disable import/no-anonymous-default-export */
import AuthApiClient from 'App/Utils/AuthApiClient';

const login = (payload: any): Promise<any> =>
  AuthApiClient.post('/signin', payload)
    .then(result => result?.data)
    .catch(error => error?.response?.data);

const logout = (): Promise<any> =>
  AuthApiClient.withAuth().then(api =>
    api
      .post('/logout')
      .then(result => result?.data)
      .catch(error => error?.response),
  );

const myProfile = (): any => {
  return {
    name: 'test',
  };
};

export default {
  login,
  logout,
  myProfile,
};
