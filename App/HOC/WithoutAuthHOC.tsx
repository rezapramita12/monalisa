import { Component } from 'react';
import { NextComponentType, NextPageContext } from 'next';
import Router from 'next/router';

import * as AuthService from 'App/Helpers/Auth';
import LoadingLayout from 'App/Layouts/Loading/WithoutAuth/Index';

export default function withoutAuth(UnauthComponent: NextComponentType) {
  const TempClass = class Unauthenticated extends Component<any, any> {
    static async getInitialProps(ctx: NextPageContext) {
      // Check if Page has a `getInitialProps`; if so, call it.
      const pageProps =
        UnauthComponent.getInitialProps && (await UnauthComponent.getInitialProps(ctx));
      // Return props.
      return { ...pageProps };
    }

    constructor(props: any) {
      super(props);
      this.state = {
        isLoading: true,
      };
    }

    componentDidMount() {
      if (AuthService.isLoggedIn()) {
        Router.replace('/admin');
      } else {
        this.setState({ isLoading: false });
      }
    }

    render() {
      return (
        <div>{this.state.isLoading ? <LoadingLayout /> : <UnauthComponent {...this.props} />}</div>
      );
    }
  };

  return TempClass;
}
