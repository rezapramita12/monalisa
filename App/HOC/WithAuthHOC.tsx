import { Component } from 'react';
import { NextComponentType, NextPageContext } from 'next';
import Router from 'next/router';

import * as AuthService from 'App/Helpers/Auth';
import LoadingLayout from 'App/Layouts/Loading/WithAuth/Index';

export default function withAuth(AuthComponent: NextComponentType) {
  const TempClass = class Authenticated extends Component<any, any> {
    static async getInitialProps(ctx: NextPageContext) {
      // Check if Page has a `getInitialProps`; if so, call it.
      const pageProps = AuthComponent.getInitialProps && (await AuthComponent.getInitialProps(ctx));
      // Return props.
      return { ...pageProps };
    }

    constructor(props: any) {
      super(props);
      this.state = {
        isLoading: true,
      };
    }

    componentDidMount() {
      if (false) {
        Router.replace('/login');
      } else {
        this.setState({ isLoading: false });
      }
    }

    render() {
      return (
        <div>{this.state.isLoading ? <LoadingLayout /> : <AuthComponent {...this.props} />}</div>
      );
    }
  };

  return TempClass;
}
