import { Global, css } from '@emotion/react';

// Reset default browser styling
const GlobalStyle = () => {
  return <Global styles={globalStyles} />;
};

const globalStyles = css`
  * {
    font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial,
      Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
  }
`;

export default GlobalStyle;
