/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */


import * as React from 'react';
import RouterLink from 'next/link';
import Router from 'next/router';

import * as Yup from 'yup';
import { TextField } from '@mui/joy';
import { Icon } from '@iconify/react';
import { useSnackbar } from 'notistack';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import eyeFill from '@iconify/icons-eva/eye-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import closeFill from '@iconify/icons-eva/close-fill';
import { Form, FormikProvider, useFormik } from 'formik';
import {
  Alert,
  Card,
  IconButton,
  InputAdornment,
  Stack,
  Link,
  Button,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import { LoadingButton } from '@mui/lab';

import * as AuthService from '@/Helpers/Auth';

type InitialValues = {
  email: string;
  password: string;
  afterSubmit?: string;
};

const StyledForm = styled('div')(({ theme }) => ({
  padding: 50,
}));

const StyledCard = styled(Card)(({ theme }) => ({
  maxWidth: 500,
  boxShadow: '4px 4px 12px 3px rgb(45 156 219 / 20%)',
  borderRadius: 8,
}));

const CenteredImage = styled('div')(({ theme }) => ({
  textAlign: 'center',
  paddingBottom: 40,
}));

const RootStyle = styled('div')(({ theme }) => ({
  [theme.breakpoints.up('md')]: {
    display: 'flex',
  },
}));

const SectionStyle = styled('div')(({ theme }) => ({
  width: '100%',
  maxWidth: 695,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  margin: theme.spacing(5, 0, 2, 12),
}));

const ContentStyle = styled('div')(({ theme }) => ({
  maxWidth: 480,
  margin: 'auto',
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column',
  justifyContent: 'center',
}));

const StyledTextField = styled(TextField)(({ theme }) => ({
  input: {
    '&::placeholder': {
      fontSize: 12,
    },
  },
}));

export default function LoginForm() {
  const [showPassword, setShowPassword] = React.useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const loginSchema = Yup.object().shape({
    email: Yup.string()
      .email('Format nama pengguna salah')
      .required('Nama pengguna tidak boleh kosong'),
    password: Yup.string().required('Kata sandi tidak boleh kosong'),
  });

  const formik = useFormik<InitialValues>({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: loginSchema,
    onSubmit: async (values, { setErrors, setSubmitting }) => {
      setSubmitting(true);
      const responseData = await AuthService.login({
        email: values.email,
        password: values.password,
      });
      const isError: any = responseData?.statusCode != 200;
      if (isError) {
        let errMsg = responseData?.message;
        if (responseData?.statusCode == 401) {
          errMsg = 'Nama pengguna atau kata sandi salah';
        } else if (responseData?.statusCode == 404) {
          errMsg = 'Pengguna tidak ditemukan';
        }
        enqueueSnackbar(errMsg, {
          variant: 'error',
          action: key => (
            <Button size="small" onClick={() => closeSnackbar(key)}>
              <Icon color="black" icon={closeFill} />
            </Button>
          ),
        });
        setSubmitting(false);
      } else {
        setSubmitting(false);
        enqueueSnackbar('Login Berhasil!', {
          variant: 'success',
          action: key => (
            <Button size="small" onClick={() => closeSnackbar(key)}>
              <Icon color="black" icon={closeFill} />
            </Button>
          ),
        });
        Router.replace('/admin');
      }
    },
  });

  const { errors, touched, values, isSubmitting, handleSubmit, getFieldProps } = formik;

  const handleShowPassword = () => {
    setShowPassword(show => !show);
  };

  return (
    <RootStyle>

      <Container maxWidth="sm">
        <ContentStyle>
          <StyledCard>
            <StyledForm>
              <FormikProvider value={formik}>
                <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
                  <CenteredImage>
                    <Typography
                      variant="h6"
                      sx={{
                        paddingBottom: 1,
                        fontWeight: 700,
                        fontFamily: 'Roboto',
                        color: '#606060',
                        fontSize: '1.5rem',
                      }}
                    >
                      Hi,Selamat Datang Kembali
                    </Typography>
                    <Typography
                      variant="h6"
                      sx={{
                        paddingBottom: 1,
                        fontFamily: 'Roboto',
                        color: '#BDBDBD',
                        fontSize: '1rem',
                      }}
                    >
                      Mulai perjalanan anda disini
                    </Typography>
                  </CenteredImage>


                  <Stack spacing={2}>
                    {errors.afterSubmit && <Alert severity="error">{errors.afterSubmit}</Alert>}
                    <StyledTextField
                      size="sm"
                      componentsProps={{
                        label: {
                          style: {
                            fontWeight: 'normal',
                          },
                        },
                        input: {
                          style: {
                            borderRadius: '0.25rem',
                            borderColor: '#DDDDDD',
                            color: '#606060',
                            outline: '0px !important',
                            WebkitAppearance: 'none',
                            boxShadow: 'none !important',
                            paddingLeft: '1rem',
                            backgroundClip: 'padding-box',
                            appearance: 'none',
                            transition:
                              'border-color .15s ease-in-out, box-shadow .15s ease-in-out',
                          },
                        },
                      }}
                      fullWidth
                      autoComplete="email"
                      placeholder="Masukkan Nama Pengguna"
                      type="text"
                      label="Username"
                      {...getFieldProps('email')}
                      error={Boolean(touched.email && errors.email)}
                      helperText={touched.email && errors.email}
                    />

                    <StyledTextField
                      size="sm"
                      componentsProps={{
                        label: {
                          style: {
                            fontWeight: 'normal',
                          },
                        },
                        input: {
                          style: {
                            borderRadius: '0.25rem',
                            borderColor: '#DDDDDD',
                            color: '#606060',
                            outline: '0px !important',
                            WebkitAppearance: 'none',
                            boxShadow: 'none !important',
                            paddingLeft: '1rem',
                            backgroundClip: 'padding-box',
                            appearance: 'none',
                            transition:
                              'border-color .15s ease-in-out, box-shadow .15s ease-in-out',
                          },
                        },
                      }}
                      fullWidth
                      autoComplete="current-password"
                      type={showPassword ? 'text' : 'password'}
                      label="Password"
                      placeholder="Masukan Kata Sandi"
                      {...getFieldProps('password')}
                      endDecorator={
                        <InputAdornment position="end">
                          <IconButton onClick={handleShowPassword} edge="end">
                            <Icon icon={showPassword ? eyeFill : eyeOffFill} />
                          </IconButton>
                        </InputAdornment>
                      }
                      error={Boolean(touched.password && errors.password)}
                      helperText={touched.password && errors.password}
                    />
                    <Typography sx={{ fontSize: 12, pb: 2 }}>
                      <Link component={RouterLink} variant="subtitle2" href={'/lupa-password'}>
                        Lupa Kata Sandi?
                      </Link>
                    </Typography>
                    <LoadingButton
                      fullWidth
                      size="large"
                      type="submit"
                      variant="contained"
                      loading={isSubmitting}
                      sx={{ fontSize: 12, textTransform: 'none' }}
                    >
                      Masuk
                    </LoadingButton>
                    <Typography component={'span'} sx={{ textAlign: 'center', fontSize: 12 }}>
                      {'Tidak mempunyai akun? '}
                      <Link component={RouterLink} variant="subtitle2" href={'/daftar'}>
                        Daftar
                      </Link>
                    </Typography>
                  </Stack>
                </Form>
              </FormikProvider>
            </StyledForm>
          </StyledCard>
        </ContentStyle>
      </Container>
      <SectionStyle>
        <img src={'/img/logo-monalisa.svg'} />
      </SectionStyle>
    </RootStyle>
  );
}
