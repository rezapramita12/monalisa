import { Grid } from '@mui/material';
import DHChart from 'App/Components/Chart/DHChart';

const Chart = () => {
  const config = {
    type: 'line',
    data: {
      labels: ['8 Juli', '9 Juli', '10 Juli', '11 Juli', '12 Juli', '13 Juli'],
      datasets: [
        {
          label: new Date().getFullYear(),
          borderColor: '#34BFA3',
          data: [65, 78, 66, 44, 56, 67, 75],
          fill: false,
          tension: 0.1,
        },
      ],
    },
    options: {
      maintainAspectRatio: false,
      responsive: true,
      layout: {
        autoPadding: true,
      },
      title: {
        display: false,
        text: 'Sales Charts',
        fontColor: 'white',
      },
      legend: {
        display: false,
      },
      tooltips: {
        mode: 'index',
        intersect: false,
      },
      hover: {
        mode: 'nearest',
        intersect: true,
      },
      scales: {
        yAxes: [
          {
            gridLines: {
              drawBorder: false,
              borderDash: [5, 5],
            },
            ticks: {
              min: 0,
              max: 100,
              ticks: {
                stepSize: 10,
                padding: 10,
              },
            },
          },
        ],
        xAxes: [
          {
            gridLines: {
              display: false,
              drawBorder: false,
              lineWidth: 0,
            },
            ticks: {
              padding: 10,
            },
          },
        ],
      },
    },
  };
  return (
    <Grid container rowSpacing={4} columnSpacing={2}>
      <Grid item xs={12} md={6}>
        <DHChart title="Pendapatan Minggu Ini" id="line-chart" config={config} />
      </Grid>
      <Grid item xs={12} md={6}>
        <DHChart title="Pendapatan Bulan Ini" id="line-chart-2" config={config} />
      </Grid>
    </Grid>
  );
};

export default Chart;
