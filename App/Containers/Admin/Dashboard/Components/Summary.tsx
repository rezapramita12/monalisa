import { Grid } from '@mui/material';
import SummaryTableCard from 'App/Components/Card/SummaryTableCard';

const Summary = () => {
  const tableHeader = ['1 Bulan', '3 Bulan', '6 Bulan'];
  const tableContent = [203, 203, 203];
  const tableHaderDokter = ['Umum', 'Kandungan', 'Penyakit Dalam'];
  return (
    <Grid container rowSpacing={1}>
      <Grid item xs={12}>
        <SummaryTableCard
          src="/img/icons/icon-medicines.svg"
          title="Obat Kadaluarsa"
          url="/more-obat"
          tableHeader={tableHeader}
          tableContent={tableContent}
        />
      </Grid>
      <Grid item xs={12}>
        <SummaryTableCard
          src="/img/icons/icon-dokter.svg"
          title="Dokter"
          url="/more-dokter"
          tableHeader={tableHaderDokter}
          tableContent={tableContent}
        />
      </Grid>
    </Grid>
  );
};

export default Summary;
