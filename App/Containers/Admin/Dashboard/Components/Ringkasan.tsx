import Typography from '@mui/material/Typography';
import { Card, Grid } from '@mui/material';
import { styled } from '@mui/material/styles';
import SignalCellularAltIcon from '@mui/icons-material/SignalCellularAlt';
import theme from 'App/Theme';
import BoxPercentage from 'App/Components/Box/BoxPercentage';

const DashboardWrapper = styled('div')(({ theme }) => ({
  paddingTop: 20,
  paddingLeft: 50,
  paddingRight: 50,
  paddingBottom: 50,
}));

const StyledTitle = styled('p')(({ theme }) => ({
  display: 'inline',
  fontSize: 18,
  verticalAlign: 'middle',
  fontWeight: 700,
  paddingLeft: 2,
}));
const CardBody = styled('div')(({ theme }) => ({
  paddingTop: 20,
  paddingBottom: 20,
}));

const StyledImg = styled('img')(({ theme }) => ({
  // height: '100%',
  width: '2rem',
}));

const Ringkasan = () => {
  return (
    <Card sx={{ paddingLeft: 3, paddingRight: 3 }}>
      <Grid container>
        <StyledImg src={'/img/icons/icon-ringkasan.svg'} />
        <StyledTitle>Ringkasan</StyledTitle>
      </Grid>
      <Typography variant="subtitle1">Ringkasan Anda selama</Typography>
      <CardBody>
        <Grid container columnSpacing={1} rowSpacing={2}>
          <Grid item md={4} xs={12}>
            <BoxPercentage
              src="/img/icons/icon-rawat-jalan.svg"
              title="Rawat Jalan"
              amount={302}
              percentage={80}
              isUp={true}
            />
          </Grid>
          <Grid item md={4} xs={12}>
            <BoxPercentage
              src="/img/icons/icon-batal.svg"
              title="Batal"
              amount={302}
              percentage={80}
              isUp={false}
            />
          </Grid>
          <Grid item md={4} xs={12}>
            <BoxPercentage
              src="/img/icons/icon-tebus.svg"
              title="Tebus Resep"
              amount={302}
              percentage={80}
              isUp={true}
            />
          </Grid>
        </Grid>
      </CardBody>
    </Card>
  );
};

export default Ringkasan;
