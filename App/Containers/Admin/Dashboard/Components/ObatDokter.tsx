import { Card, Grid } from '@mui/material';
import { styled } from '@mui/material/styles';
import theme from 'App/Theme';
import BoxWithoutPercentage from 'App/Components/Box/BoxWithoutPercentage';

const ObatDokter = () => {
  return (
    <Grid container columnSpacing={3}>
      <Grid item sm={6} xs={12}>
        <Card>
          <BoxWithoutPercentage
            src="/img/icons/icon-rawat-jalan.svg"
            title="Rawat Jalan"
            amount={302}
          />
        </Card>
      </Grid>
      <Grid item sm={6} xs={12}>
        <Card>
          <BoxWithoutPercentage
            src="/img/icons/icon-pendaftaran.svg"
            title="Reservasi"
            amount={302}
          />
        </Card>
      </Grid>
    </Grid>
  );
};

export default ObatDokter;
