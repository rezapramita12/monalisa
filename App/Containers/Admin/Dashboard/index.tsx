import { Grid } from '@mui/material';
import { styled } from '@mui/material/styles';
import Ringkasan from './Components/Ringkasan';
import ObatDokter from './Components/ObatDokter';
import Summary from './Components/Summary';
import Chart from './Components/Chart';

const DashboardWrapper = styled('div')(({ theme }) => ({
  paddingTop: 20,
  paddingLeft: 50,
  paddingRight: 50,
  paddingBottom: 50,
}));

const Dashboard = () => {
  return (
    <DashboardWrapper>
      <Grid container columnSpacing={5} rowSpacing={4}>
        <Grid item xs={12} lg={7}>
          <Grid container rowSpacing={2}>
            <Grid item xs={12}>
              <Ringkasan />
            </Grid>
            <Grid item xs={12}>
              <ObatDokter />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} lg={5}>
          <Summary />
        </Grid>
        <Grid item xs={12}>
          <Chart />
        </Grid>
      </Grid>
    </DashboardWrapper>
  );
};
export default Dashboard;
