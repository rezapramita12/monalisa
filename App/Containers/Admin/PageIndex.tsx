import * as React from 'react';
import type { NextPage } from 'next';

import DhNavbar from 'App/Layouts/Navbar';
import DhTopbar from 'App/Layouts/Topbar';
import Dashboard from 'App/Containers/Admin/Dashboard';
import AppLayout from '@/Layouts/AppLayout/Index';

const PageComponent: NextPage = (props: any) => {
  const { sections, title } = props;
  return (
    <React.Fragment>
      <AppLayout>
        <Dashboard />
      </AppLayout>
    </React.Fragment>
  );
};

export default PageComponent;
