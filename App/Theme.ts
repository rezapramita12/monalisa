import { experimental_extendTheme as extendMuiTheme } from '@mui/material/styles';
import { red } from '@mui/material/colors';

// Create a theme instance.
const theme = extendMuiTheme({
  colorSchemes: {
    light: {
      palette: {
        text: {
          primary: '#606060',
          disabled: '#DDDDDD',
        },
        primary: {
          main: '#2F80ED',
        },
        secondary: {
          main: '#0E51AA',
        },
        error: {
          main: red.A400,
        },
        background: {
          default: '#f5f5f5',
        },
        common: {
          black: '#212529',
          white: '#ffffff',
        },
      },
    },
  },
  typography: {
    fontSize: 12,
  },
});

export default theme;
