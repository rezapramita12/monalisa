/**
 * @author Andri Amirul (andri.amirul@dhealth.co.id)
 * A Product of PT Citraraya Nusatama
 * Powered by D'Health
 */
import withoutAuth from '@/HOC/WithoutAuthHOC';
import LoginForm from '@/Containers/Authenthication/Login/LoginForm';

const Login = () => {
  return <LoginForm />;
};

function WrappedComponent(props: any) {
  const AuthenticatedComponent = withoutAuth(Login);
  return <AuthenticatedComponent {...props} />;
}

export default WrappedComponent;
