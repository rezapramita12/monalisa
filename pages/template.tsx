import { FC, Fragment } from 'react';

import DhButton from 'App/Components/Button';

const Template: FC = () => {
  return (
    <div style={{ padding: '12px' }}>
      <h1>Template</h1>
      <h2>Button</h2>
      <DhButton>Primary</DhButton>
    </div>
  );
};

export default Template;
