import React, { FC } from 'react';
import { NextComponentType, NextPageContext } from 'next';

import withAuth from 'App/HOC/WithAuthHOC';

const TestComponent: FC<any> = () => {
  return <p>Hello World</p>;
};

function ProductsPage(props: any) {
  const AuthenticatedComponent = withAuth(TestComponent);
  return <AuthenticatedComponent {...props} />;
}

export default ProductsPage;
