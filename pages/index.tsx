import type { NextPage } from 'next';

import PageComponent from 'App/Containers/Admin/PageIndex';
import withAuth from 'App/HOC/WithAuthHOC';

const Page: NextPage = () => {
  return <PageComponent />;
};

function WrappedComponent(props: any) {
  const AuthenticatedComponent = withAuth(Page);
  return <AuthenticatedComponent {...props} />;
}

export default WrappedComponent;
