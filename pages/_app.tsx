import React from 'react';
import Head from 'next/head';
import { AppProps } from 'next/app';
import { useRouter } from 'next/router';
import { Provider } from 'react-redux';
import Router from 'next/router';
import NProgress from 'nprogress';
import { deepmerge } from '@mui/utils';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { CacheProvider, EmotionCache } from '@emotion/react';

import theme from 'App/Theme';
import loginTheme from 'App/LoginTheme';
import store from 'App/Redux/Store';
import GlobalStyle from 'App/Styles/GlobalStyle';
import createEmotionCache from 'App/CreateEmotionCache';
import NotistackProvider from 'App/Components/Notistack/NotistackProvider';
import { CssVarsProvider } from '@mui/joy';
import joyTheme from 'App/JoyTheme';

import 'nprogress/nprogress.css';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { ProSidebarProvider } from 'react-pro-sidebar';

// Client-side cache, shared for the whole session of the user in the browser
const clientSideEmotionCache = createEmotionCache();

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
}

export default function MyApp(props: MyAppProps) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
  const router = useRouter();
  let dynamicTheme = theme;
  if (router.pathname && router.pathname.startsWith('/login')) dynamicTheme = loginTheme;
  const mergedTheme = deepmerge(joyTheme, dynamicTheme);

  React.useEffect(() => {
    const handleRouteStart = () => NProgress.start();
    const handleRouteDone = () => NProgress.done();
    Router.events.on('routeChangeStart', handleRouteStart);
    Router.events.on('routeChangeComplete', handleRouteDone);
    Router.events.on('routeChangeError', handleRouteDone);
    return () => {
      // Make sure to remove the event handler on unmount!
      Router.events.off('routeChangeStart', handleRouteStart);
      Router.events.off('routeChangeComplete', handleRouteDone);
      Router.events.off('routeChangeError', handleRouteDone);
    };
  }, []);

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <title>Monalisa</title>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
        <meta charSet="utf-8" />
        <meta name="title" content="Cliniqu" />
        <meta name="keywords" content="klinik, kesehatan" />
        <meta name="description" content="Cliniqu" />
        <meta name="author" content="DHealth" />
        <meta name="language" content="Bahasa Indonesia" />
        <meta name="robots" content="index, follow" />
        <meta name="msapplication-TileColor" content="#202020" />
        <meta name="theme-color" content="#202020" />
      </Head>
      <NotistackProvider>
        <Provider store={store}>
          <CssVarsProvider theme={mergedTheme}>
            {/* <ThemeProvider theme={dynamicTheme}> */}
            <CssBaseline />
            <GlobalStyle />
            <LocalizationProvider dateAdapter={AdapterMoment}>
              <ProSidebarProvider>

                <Component {...pageProps} />
              </ProSidebarProvider>

            </LocalizationProvider>
            {/* </ThemeProvider> */}
          </CssVarsProvider>
        </Provider>
      </NotistackProvider>
    </CacheProvider>
  );
}
