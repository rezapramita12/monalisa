import * as React from 'react';
import type { NextPage } from 'next';
import { useDispatch, useSelector } from 'react-redux';

import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

import Link from 'App/Link';
import ProTip from 'App/Protip';
import Copyright from 'App/Copyright';
import { getUser, setUser } from 'App/Redux/Slices/UserSlice';

const SampleRedux: NextPage = () => {
  const dispatch = useDispatch();
  const user = useSelector(getUser);

  React.useEffect(() => {
    setTimeout(() => {
      dispatch(
        setUser({
          id: 2,
          username: 'Kevin',
        }),
      );
    }, 2000);
  });

  return (
    <Container maxWidth="lg">
      <Box
        sx={{
          my: 4,
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Typography variant="h4" component="h1" gutterBottom>
          MUI v5 + Next.js with TypeScript example
        </Typography>
        <Link href="/about" color="secondary">
          Go to the about page Test Redux Username : {user.username}
        </Link>
        <ProTip />
        <Copyright />
      </Box>
    </Container>
  );
};

export default SampleRedux;
