/** @type {import('next').NextConfig} */
module.exports = {
    reactStrictMode: true,
    publicRuntimeConfig: {
        APP_VERSION: process.env.APP_VERSION,
        APP_ENVIRONMENT: process.env.APP_ENVIRONMENT,
        COOKIES_DOMAIN: process.env.COOKIES_DOMAIN,
        API_URL: process.env.API_URL,
        PREFIX_URL_MASTER: process.env.PREFIX_URL_MASTER,
        MOCK_API_URL: process.env.MOCK_API_URL,
        // Auth Temporary
        AUTH_API_URL: process.env.AUTH_API_URL,
        MASTER_API_URL: process.env.MASTER_API_URL,
    },
    // eslint: {
    //     dirs: ['pages', 'App']
    // }
};
